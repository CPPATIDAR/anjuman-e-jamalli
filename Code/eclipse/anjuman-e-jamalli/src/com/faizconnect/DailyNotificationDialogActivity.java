package com.faizconnect;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.faizconnect.common.NotificationDailyThaliMenuUserView;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DailyNotificationDialogActivity extends Activity{

	private TextView tvJisManiFawaid;
	private TextView tvRuhaniFawaid;
	private LinearLayout llParentThali;
	public String thaliId;
	private ShowDialog showMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_daily_notification);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		llParentThali = (LinearLayout) findViewById(R.id.ll_today_thali_menu);
		tvJisManiFawaid = (TextView) findViewById(R.id.tv_daily_noti_jismani_fawaid);
		tvRuhaniFawaid = (TextView) findViewById(R.id.tv_daily_noti_ruhani_fawaid);
		Intent intent = getIntent();
	//	thaliId = intent.getStringExtra("THALI_ID");
		thaliId = intent.getExtras().getString("THALI_ID");

		System.out.println(" Daily noti screen thaliId"+thaliId);
		new GetThaliFeedbackAsyncTask().execute();
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
	}
	
	class GetThaliFeedbackAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("thaalimenu_id",thaliId));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(DailyNotificationDialogActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			response = CustomHttpClient.executeHttpPost(DailyNotificationDialogActivity.this,
					WebUrl.GET_THALI_FEEDBACK, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
							
						llParentThali.addView(NotificationDailyThaliMenuUserView.inflateMenu(DailyNotificationDialogActivity.this, jsonObject2, false, false,
								R.drawable.menu_button_selector,R.color.menu_button_font,false,false,false));
						
						new GetFawaidAsyncTask().execute();
						
					}else{
						showMessage.showMsg(DailyNotificationDialogActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(DailyNotificationDialogActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	class GetFawaidAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			//urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id", thaaliMenuId));
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id", thaliId));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(DailyNotificationDialogActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(DailyNotificationDialogActivity.this,
					WebUrl.ADMIN_FAWAID, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObject=jsonArray.optJSONObject(i);
							tvJisManiFawaid.setText(jObject.optString("jismani_fawaid"));
							tvRuhaniFawaid.setText(jObject.optString("ruhani_fawaid"));
							
							 getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						}
					}
				} else {
					Toast.makeText(DailyNotificationDialogActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
