package com.faizconnect;

import android.app.Application;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.SPParse;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class FaizConnect extends Application {

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		Parse.initialize(getApplicationContext(),
				getString(R.string.parse_app_id),
				getString(R.string.parse_client_key));
		PushService.setDefaultPushCallback(getApplicationContext(),
				null);

		try {
			ParseInstallation tmp = ParseInstallation.getCurrentInstallation();
			Log.v("regId Parse", 
					tmp.getInstallationId());
			/*SPParse.setValue(getApplicationContext(),
					SPParse.Parse_Installation_ID, ParseInstallation
							.getCurrentInstallation().getInstallationId());*/

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
