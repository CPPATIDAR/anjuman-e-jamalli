package com.faizconnect;

import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.faizconnect.adminmodule.AdminDashboardActivity;
import com.faizconnect.usermodule.UserDashboardActivity;
import com.faizconnect.utill.SPParse;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(com.faizconnect.utill.WebUrl.SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		Log.v("regId===", registrationId);
		CommonUtilities.displayMessage(context,
				"Your device registred with GCM");
		SPParse.setValue(getApplicationContext(),
				SPParse.Parse_Installation_ID, registrationId);
		// ServerUtilities.register(context, MainActivity.name);
	}

	/**
	 * Method called on device un registred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		// Toast.makeText(context, "onMessage", Toast.LENGTH_LONG).show();
		try {

			if (UserDashboardActivity.activity != null) {
				UserDashboardActivity.activity.refreshCount();
			}
			if (AdminDashboardActivity.activty != null) {
				AdminDashboardActivity.activty.refresh();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			Log.i(TAG, "Received message");
			System.out.println("NOTI===" + intent.getExtras() + "");
			String message = intent.getExtras().getString("message");
			CommonUtilities.displayMessage(context, message);
			// notifies user
			generateNotification(context, intent);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		// Toast.makeText(context, "onDeletedMessages",
		// Toast.LENGTH_LONG).show();
		String message = getString(R.string.gcm_deleted, total);
		CommonUtilities.displayMessage(context, message);
		// notifies user
		// generateNotification(context, message);
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		// Toast.makeText(context, "onError", Toast.LENGTH_LONG).show();
		Log.i(TAG, "Received error: " + errorId);
		CommonUtilities.displayMessage(context,
				getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		CommonUtilities.displayMessage(context,
				getString(R.string.gcm_recoverable_error, errorId));
		// Toast.makeText(context, "onRecoverableError",
		// Toast.LENGTH_LONG).show();
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, Intent i) {
		// Toast.makeText(context, "generateNotification",
		// Toast.LENGTH_LONG).show();
		String message = i.getExtras().getString("message");
		// String commentId = i.getExtras().getString("comment_id"); /// Ask
		// Varshit unusual code
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = i.getExtras().getString("price");
		
		String thali_menu_id =i.getExtras().getString("thali_menu_id");
		System.out.println("thali_menu_id=="+thali_menu_id);

		Intent notificationIntent;
//		notificationIntent = new Intent(context, SplashActivity.class);
		
				
		if (thali_menu_id != null) 
		{
			notificationIntent = new Intent(context, DailyNotificationDialogActivity.class)
					.putExtra("THALI_ID", thali_menu_id);
		}else {
			notificationIntent = new Intent(context, SplashActivity.class);
		}
		

		/*
		 * if(commentId !=null) { notificationIntent = new Intent(context,
		 * SplashActivity.class).putExtra("COMMENT_ID", commentId); }
		 */

		notificationIntent.putExtra("notification_data", message);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(new Random().nextInt(), notification);

	}

}
