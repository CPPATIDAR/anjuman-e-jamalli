package com.faizconnect;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.faizconnect.adminmodule.AdminDashboardActivity;
import com.faizconnect.usermodule.UserDashboardActivity;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.SPParse;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class LoginActivity extends Activity{
	
	private EditText etUserName;
	private EditText etToDate;
	private Button btnLogin;
	private ShowDialog showDialog;
	private String deviceId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showDialog = new ShowDialog();
		etUserName = (EditText) findViewById(R.id.et_username);
		etToDate = (EditText) findViewById(R.id.et_to_dat);
		btnLogin = (Button) findViewById(R.id.btn_login);
		TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();
		
		//showDialog.showMsg(LoginActivity.this, "If you are login first time in .... app, then your password is last 4 digits of HOF Ejamaat Id.");
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etUserName.getText().toString().length() == 0) {
					showDialog.showMsg(LoginActivity.this, "Please Enter E-Jamaat Id");
				}else if (etUserName.getText().toString().length() !=8) {
					showDialog.showMsg(LoginActivity.this, "Please Enter Valid HOF Ejamaat Id");
				}else if (etToDate.getText().toString().length() == 0) {
					showDialog.showMsg(LoginActivity.this, "Please Enter To Password");
				} else {
					new LoginAsyncTask().execute();
				}
				
//				new LoginAsyncTask().execute();
				
			}
		});
	}
	
	class LoginAsyncTask extends AsyncTask<Void, Void, Void>{

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("ejamaat_id",etUserName.getText().toString()));
			urlParameter.add(new BasicNameValuePair("password",etToDate.getText().toString()));
			urlParameter.add(new BasicNameValuePair("device_id",deviceId));
			urlParameter.add(new BasicNameValuePair("device_token",SPParse.getValue(LoginActivity.this, SPParse.Parse_Installation_ID)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(LoginActivity.this);   //   Create Dialog
			progressDialog.setMessage("Loading...");				   //   Show message in dialog box
			progressDialog.show();	
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(LoginActivity.this, WebUrl.LOGIN,
					urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						jsObject = jsonObject.optJSONObject("result");
						
						SPAnjuman.setBooleanValue(LoginActivity.this, SPAnjuman.ACCOUNT_ACTIVE, false);
						SPAnjuman.setBooleanValue(LoginActivity.this, SPAnjuman.LoginStatus, true);
						SPAnjuman.setValue(LoginActivity.this, SPAnjuman.UserId, jsObject.optString("user_id"));
						SPAnjuman.setValue(LoginActivity.this, SPAnjuman.MohallaId, jsObject.optString("mohalla_id"));
						SPAnjuman.setValue(LoginActivity.this, SPAnjuman.ROLE_ID, jsObject.optString("role_id"));
						SPAnjuman.setValue(LoginActivity.this, SPAnjuman.EJamaatId, jsObject.optString("ejamaat_id"));
						SPAnjuman.setValue(LoginActivity.this, SPAnjuman.CURRENCY, jsObject.optString("currency"));
						System.out.println(SPAnjuman.getValue(LoginActivity.this, SPAnjuman.CURRENCY));
						new AppSettingAsyncTask().execute();
						
						
					}else {
						showDialog.showMsg(LoginActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(LoginActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	class AppSettingAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private JSONObject jsObject;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(LoginActivity.this,
					WebUrl.APP_SETTING, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (response != null) {
				
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						jsObject = jsonObject.optJSONObject("result");
						SPAnjuman.setIntValue(LoginActivity.this,
								SPAnjuman.HijriMonth,
								jsObject.optInt("hijari_month"));
						SPAnjuman.setIntValue(LoginActivity.this,
								SPAnjuman.HijriYear,
								jsObject.optInt("hijari_year"));

					}
					if (SPAnjuman.getValue(LoginActivity.this,
							SPAnjuman.ROLE_ID).equalsIgnoreCase("3")) {
						startActivity(new Intent(LoginActivity.this,
								UserDashboardActivity.class));
						finish();
					} else if (SPAnjuman.getValue(LoginActivity.this,
							SPAnjuman.ROLE_ID).equalsIgnoreCase("4")) {
						startActivity(new Intent(LoginActivity.this,
								AdminDashboardActivity.class));
						finish();
					} 
				} else {
					Toast.makeText(LoginActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
