package com.faizconnect;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.faizconnect.adminmodule.AdminDashboardActivity;
import com.faizconnect.imageloader.FileCache;
import com.faizconnect.usermodule.UserDashboardActivity;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.NetworkConnection;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.SPParse;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;
import com.google.android.gcm.GCMRegistrar;

public class SplashActivity extends Activity {

	private ShowDialog showMessage;
	private String regId;
	private String deviceId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		showMessage = new ShowDialog();

		TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();
		if (!SPAnjuman.getValue(SplashActivity.this, SPAnjuman.UserId).equalsIgnoreCase("")) 
		{
			
			new UpdateDeviceTokenAsyncTask().execute();
			
		}
		
		new AppSettingAsyncTask().execute();

		if (SPAnjuman.getBooleanValue(SplashActivity.this,
				SPAnjuman.LoginStatus)) {

			new UserStatusAsyncTask().execute();

		} else {

			if (SPAnjuman.getBooleanValue(SplashActivity.this,
					SPAnjuman.ACCOUNT_ACTIVE)) {
				new UserStatusAsyncTask().execute();
			} else {

				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {

						if (SPAnjuman.getValue(SplashActivity.this,
								SPAnjuman.ROLE_ID).equalsIgnoreCase("3")) {
							startActivity(new Intent(SplashActivity.this,
									UserDashboardActivity.class));
							finish();
						} else if (SPAnjuman.getValue(SplashActivity.this,
								SPAnjuman.ROLE_ID).equalsIgnoreCase("4")) {
							startActivity(new Intent(SplashActivity.this,
									AdminDashboardActivity.class));
							finish();
						} else {
							startActivity(new Intent(SplashActivity.this,
									LoginActivity.class));
							finish();
						}
					}
				}, 6000);

			}

		}

	}



	class UserStatusAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ProgressDialog progressDialog;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(SplashActivity.this); // Create
																		// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show(); // box
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(SplashActivity.this, SPAnjuman.UserId)));
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(SplashActivity.this,
					WebUrl.USER_ACTIVATE_STATUS, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						// finish();
						SPAnjuman.setBooleanValue(SplashActivity.this,
								SPAnjuman.LoginStatus, true);

						if (SPAnjuman.getValue(SplashActivity.this,
								SPAnjuman.ROLE_ID).equalsIgnoreCase("3")) {
							startActivity(new Intent(SplashActivity.this,
									UserDashboardActivity.class));
							finish();
						} else if (SPAnjuman.getValue(SplashActivity.this,
								SPAnjuman.ROLE_ID).equalsIgnoreCase("4")) {
							startActivity(new Intent(SplashActivity.this,
									AdminDashboardActivity.class));
							finish();
						} else {
							startActivity(new Intent(SplashActivity.this,
									LoginActivity.class));
							finish();
						}

					} else {
						SPAnjuman.clear(SplashActivity.this);
						new AlertDialog.Builder(SplashActivity.this)
								.setMessage(jsonObject.optString("message"))
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												startActivity(new Intent(
														SplashActivity.this,
														LoginActivity.class));
												dialog.cancel();
											}
										}).create().show();

					}
				} else {
					Toast.makeText(SplashActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	
	
	class UpdateDeviceTokenAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ProgressDialog progressDialog;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(SplashActivity.this); // Create
																		// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show(); // box
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("device_id", deviceId));
			urlParameter.add(new BasicNameValuePair("device_token", SPParse.getValue(SplashActivity.this, SPParse.Parse_Installation_ID)));
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman.getValue(SplashActivity.this, SPAnjuman.UserId)));
		
			
			
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(SplashActivity.this,
					WebUrl.UPDATE_DEVICE_TOKEN, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						// finish();
					

					}
				} else {
					Toast.makeText(SplashActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	
	
	class AppSettingAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private JSONObject jsObject;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(SplashActivity.this,
					WebUrl.APP_SETTING, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						jsObject = jsonObject.optJSONObject("result");
						SPAnjuman.setIntValue(SplashActivity.this,
								SPAnjuman.HijriMonth,
								jsObject.optInt("hijari_month"));
						SPAnjuman.setIntValue(SplashActivity.this,
								SPAnjuman.HijriYear,
								jsObject.optInt("hijari_year"));

					}
				} else {
					Toast.makeText(SplashActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new FileCache(SplashActivity.this).clear();
		if ( new NetworkConnection().isOnline(SplashActivity.this)) {

			// Make sure the device has the proper dependencies.
			GCMRegistrar.checkDevice(this);

			// Make sure the manifest was properly set - comment out this line
			// while developing the app, then uncomment it when it's ready.
			GCMRegistrar.checkManifest(this);

			

			// Get GCM registration id
			regId = GCMRegistrar.getRegistrationId(this);
			Log.v("regId", regId);
			if(regId!=null)
			{
				if(regId.trim().length()>0)
				{
					SPParse.setValue(getApplicationContext(),
							SPParse.Parse_Installation_ID, regId);
				}else{
					GCMRegistrar.register(this, WebUrl.SENDER_ID);
				}
			}else{
				GCMRegistrar.register(this, WebUrl.SENDER_ID);
			}
			// Check if regid already presents
		}
	}
}
