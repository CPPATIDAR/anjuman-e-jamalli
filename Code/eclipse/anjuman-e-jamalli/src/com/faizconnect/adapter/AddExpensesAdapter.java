package com.faizconnect.adapter;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminmodule.AddExpences;
import com.faizconnect.pojo.ExpensesPojo;
import com.faizconnect.pojo.UnitPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AddExpensesAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater layoutInflater;
	private ArrayList<ExpensesPojo> alExpences;
	private Button btnSave;
	protected JSONArray jsonArray;
	String thaaliMenuId = "";

	private ArrayList<ExpensesPojo> alNewExpences;
	private TextView tvTotal;
	private TextView tvAddMoreItem;
	String dialogTitle = "";
	double dialogPrice;
	double dialogQuantity;
	private ArrayList<UnitPojo> alUnit;

	public AddExpensesAdapter(Activity activity,
			ArrayList<ExpensesPojo> alExpences, String thaaliMenuId,ArrayList<UnitPojo> alUnit ) {
		super();
		this.activity = activity;
		this.alExpences = alExpences;
		this.thaaliMenuId = thaaliMenuId;
		btnSave = (Button) activity.findViewById(R.id.btn_save_expenses);
		tvTotal = (TextView) activity.findViewById(R.id.tv_total);
		tvAddMoreItem = (TextView) activity.findViewById(R.id.tv_add_more_item);
		alNewExpences = new ArrayList<ExpensesPojo>();
		this.alUnit = alUnit; 
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alExpences.size();
	}

	@Override
	public ExpensesPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alExpences.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Holder holder;

		// R.layout.no_thaali_item
		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.add_expence_item,
					null);
			holder = new Holder();
			holder.imEdit = (ImageView) convertView
					.findViewById(R.id.im_expence_edit);
			holder.imDelete = (ImageView) convertView
					.findViewById(R.id.im_expence_delete);
			holder.etTitle = (EditText) convertView
					.findViewById(R.id.et_expence_title);
			holder.etPrice = (EditText) convertView
					.findViewById(R.id.et_expence_price);
			holder.etQuantity = (EditText) convertView
					.findViewById(R.id.et_expence_quantity);
			((TextView) convertView.findViewById(R.id.tv_expences_currency)).setText(SPAnjuman.getValue(activity, SPAnjuman.CURRENCY)); 
			
			holder.etTitle.setFocusable(false);
			holder.etPrice.setFocusable(false);
			holder.etQuantity.setFocusable(false);
		} else {
			holder = (Holder) convertView.getTag();
		}
		convertView.setTag(holder);
		holder.pos = position;

		// convertView.setTag(R.id.et_expence_title,
		// alExpences.get(position).getTitle());
		holder.etTitle.setText(alExpences.get(position).getTitle());
		holder.etPrice.setText(alExpences.get(position).getPrice()+"");
		holder.etQuantity.setText(alExpences.get(position).getQuantity()+" "+alExpences.get(position).getUnit());
		/*
		 * holder.etTitle.setOnFocusChangeListener(new OnFocusChangeListener() {
		 * 
		 * @Override public void onFocusChange(View v, boolean hasFocus) { if
		 * (!hasFocus) {
		 * 
		 * 
		 * }
		 * 
		 * } });
		 */

		holder.etTitle.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length()>0) {
				alExpences.get(holder.pos).setTitle(s.toString());
				}
			}
		});

		

		holder.etPrice.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length()>0) {
				alExpences.get(holder.pos).setPrice(Double.parseDouble(s.toString()));
				}
			}
		});

		// alExpences.get(position).setTitle(holder.etTitle.getText()+"");
		/*
		 * alExpences.get(position).setPrice(holder.etPrice.getText()+"");
		 * alExpences.get(position).setQuantity(holder.etQuantity.getText()+"");
		 * 
		 * alNewExpences.get(position)
		 */

		holder.imEdit.setTag(alExpences.get(position));
		holder.imDelete.setTag(alExpences.get(position));

		holder.imDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				
				final Dialog dialog = new Dialog(activity);
				dialog.getWindow().setBackgroundDrawableResource(
						android.R.color.transparent);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.alert_dialog);
				
				 TextView tvTitle = (TextView) dialog
						.findViewById(R.id.tv_title);

				Button btnOk = (Button) dialog
						.findViewById(R.id.btn_ok);

				Button btnCancel = (Button) dialog
						.findViewById(R.id.btn_cancel);

				tvTitle.setText(activity.getString(R.string.delete_this_item_from_thaali));
				btnOk.setTag(v.getTag());
				btnOk.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						alExpences.remove(v.getTag());
						notifyDataSetChanged();
						double total = 0;
						for (int i = 0; i < alExpences.size(); i++) {
							total+=alExpences.get(i).getPrice();
						}
						tvTotal.setText(total+"");
						dialog.dismiss();
					}
				});
				btnCancel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						dialog.dismiss();
					}
				});
				dialog.show();
			

			}
		});

		holder.imEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				final Dialog dialog = new Dialog(activity);
				dialog.getWindow().setBackgroundDrawableResource(
						android.R.color.transparent);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.add_expenses_dialog);
				final EditText etTitle = (EditText) dialog
						.findViewById(R.id.et_expence_title);
				final EditText etPrice = (EditText) dialog
						.findViewById(R.id.et_expence_price);
				((TextView) dialog.findViewById(R.id.tv_expences_currency)).setText(SPAnjuman.getValue(activity, SPAnjuman.CURRENCY));
				
				final EditText etQuantity = (EditText) dialog
						.findViewById(R.id.et_expence_quantity);
				final TextView tvUnit = (TextView) dialog
						.findViewById(R.id.tv_expence_unit);
				tvUnit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						setUnit((TextView) v);
					}

				});

				Button btnOk = (Button) dialog
						.findViewById(R.id.btn_ok_expenses);

				etTitle.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						if (s.length()>0) {
						alExpences.get(holder.pos).setTitle(s.toString());
						}
					}
				});

				etPrice.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						if (s.length()>0) {
						alExpences.get(holder.pos).setPrice(Double.parseDouble(s.toString()));
						}
					}
				});

				etQuantity.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						if (s.length()>0) {
						alExpences.get(holder.pos).setQuantity(Double.parseDouble(s.toString()));
						}
					}
				});

				// This will get values from list and set in dialog
				etTitle.setText(alExpences.get(holder.pos).getTitle());
				etPrice.setText(alExpences.get(holder.pos).getPrice()+"");
				etQuantity.setText(alExpences.get(holder.pos).getQuantity()+"");
				tvUnit.setText(alExpences.get(holder.pos).getUnit());
				UnitPojo unit = new UnitPojo();
				unit.setId(alExpences.get(holder.pos).getUnitId());
				unit.setName(alExpences.get(holder.pos).getUnit());
				tvUnit.setTag(unit);
				btnOk.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						holder.etTitle.setText(etTitle.getText().toString());
						holder.etPrice.setText(etPrice.getText().toString());
						holder.etQuantity.setText(etQuantity.getText().toString()+" "+alExpences.get(position).getUnit());
						alExpences.get(holder.pos).setUnit(tvUnit.getText().toString());
						alExpences.get(holder.pos).setUnitId(((UnitPojo)tvUnit.getTag()).getId());		
						double total = 0;
						for (int i = 0; i < alExpences.size(); i++) {
							total+=alExpences.get(i).getPrice();
						}
						tvTotal.setText(total+"");
						
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});

		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				jsonArray = new JSONArray();

				for (int i = 0; i < alExpences.size(); i++) {
					JSONObject jsonObject = new JSONObject();

					try {
						jsonObject.put("title", alExpences.get(i).getTitle()
								+ "");
						jsonObject.put("price", alExpences.get(i).getPrice());
						jsonObject.put("quantity", alExpences.get(i)
								.getQuantity());
						jsonObject.put("unit_id", alExpences.get(i)
								.getUnitId());
						jsonObject.put("unit", alExpences.get(i)
								.getUnit());
						jsonArray.put(jsonObject);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				new AddExpensesAsyncTask().execute();

			}
		});

		return convertView;
	}

	public static class Holder {

		public EditText etQuantity;
		public EditText etPrice;
		public EditText etTitle;
		public ImageView imDelete;
		public ImageView imEdit;
		int pos;
	}

	class AddExpensesAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id",
					thaaliMenuId));
			urlParameter.add(new BasicNameValuePair("data", jsonArray + ""));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(activity); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(activity,
					WebUrl.ADD_EXPENSES, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			super.onPostExecute(result);
			try {
				if (response != null) {
					// 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						Toast.makeText(activity, jsonObject.optString("message"), Toast.LENGTH_LONG).show();
						activity.finish();
						}
				} else {
					Toast.makeText(activity, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	private void setUnit(final TextView v) {

		AlertDialog.Builder myDialog = new AlertDialog.Builder(activity);

		// myDialog.set
		final ListView listview = new ListView(activity);
		LinearLayout layout = new LinearLayout(activity);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.addView(listview);
		myDialog.setView(layout);
		UnitAdapter arrayAdapter = new UnitAdapter(activity, alUnit);
		listview.setAdapter(arrayAdapter);

		final AlertDialog myalertDialog = myDialog.show();
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				v.setText(alUnit.get(position).getName());
				v.setTag(alUnit.get(position));
				myalertDialog.dismiss();
			}
		});

	}

}
