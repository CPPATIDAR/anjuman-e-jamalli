package com.faizconnect.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.faizconnect.R;
import com.faizconnect.pojo.ThaliPojo;


public class AddRecipientsAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater layoutInflater;
	private ArrayList<ThaliPojo> alRecipients;

	public AddRecipientsAdapter(Activity activity, ArrayList<ThaliPojo> alRecipients) {
		super();
		this.activity = activity;
		this.alRecipients = alRecipients;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alRecipients.size();
	}

	@Override
	public ThaliPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alRecipients.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Holder holder;

		// R.layout.no_thaali_item
		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.add_recipients_item, null);
			holder = new Holder();
			holder.tvUserName = (TextView) convertView
					.findViewById(R.id.tv_recipients_user_name);
			holder.tvCount = (TextView) convertView
					.findViewById(R.id.tv_recipients_number);
			
			holder.chkClick = (CheckBox) convertView
					.findViewById(R.id.chk_recipients_flag);
			convertView.setTag(holder);

		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvUserName.setText(alRecipients.get(position).getName().toString());
		holder.tvCount.setText(alRecipients.get(position).getThaliNumber()+"");
		
		/*if (recpID==alRecipients.get(position).getRecipientsId()) {
		holder.chkClick.setChecked(true);
	}*/
		
		
		/*holder.chkClick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(activity,SendMessageActivity.class);
				activity.startActivity(intent);
			}
		});*/
		
		if (alRecipients.get(position).getStatus()==1) {
			holder.chkClick.setChecked(true);
		}else {
			holder.chkClick.setChecked(false);
		}
		holder.chkClick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (holder.chkClick.isChecked()) {
					alRecipients.get(position).setStatus(1);
				} else {
					alRecipients.get(position).setStatus(0);
				}
			}
		});
		

		return convertView;
	}

	public static class Holder {

		TextView tvUserName;
		TextView tvCount;
		CheckBox chkClick;
	}
}
