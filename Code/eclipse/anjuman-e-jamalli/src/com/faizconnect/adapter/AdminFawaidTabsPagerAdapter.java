package com.faizconnect.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.faizconnect.adminfragment.AdminJismaniFragment;
import com.faizconnect.adminfragment.AdminRuhaniFragment;
import com.faizconnect.userfragment.UserJismaniFragment;
import com.faizconnect.userfragment.UserRuhaniFragment;

public class AdminFawaidTabsPagerAdapter extends FragmentPagerAdapter {

	public AdminFawaidTabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Jismani fragment activity
			return new AdminJismaniFragment();
		case 1:

			// Ruhani fragment activity
			return new AdminRuhaniFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
