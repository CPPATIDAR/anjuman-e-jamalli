package com.faizconnect.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import com.faizconnect.R;
import com.faizconnect.common.AdminMonthlyThaliMenuView;
import com.faizconnect.common.AdminThaliMenuView;
import com.faizconnect.common.ThaliMenuUserView;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class AdminMenuThaliAdapter extends BaseAdapter {

	private Activity activity;
	private JSONArray jsonArray;

	public AdminMenuThaliAdapter(Activity activity, JSONArray jsonArray) {
		this.activity = activity;
		this.jsonArray = jsonArray;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return jsonArray.length();
	}

	@Override
	public JSONObject getItem(int position) {
		return jsonArray.optJSONObject(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = AdminMonthlyThaliMenuView.inflateMenu(activity, getItem(position), true, true,
					R.drawable.menu_button_selector,R.color.menu_button_font,true,true,getItem(position).optBoolean("show_feedback"));
		}
		convertView = AdminMonthlyThaliMenuView.inflateMenu(activity, getItem(position), true, true,
				R.drawable.menu_button_selector,R.color.menu_button_font,true,true,getItem(position).optBoolean("show_feedback"));
		// R.layout.no_thaali_item

		return convertView;
	}
}
