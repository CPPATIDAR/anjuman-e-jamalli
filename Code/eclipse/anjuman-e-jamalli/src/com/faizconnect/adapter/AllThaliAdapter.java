package com.faizconnect.adapter;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminfragment.AllThaaliFragment;
import com.faizconnect.adminmodule.SendMessageActivity;
import com.faizconnect.pojo.ThaliPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class AllThaliAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater layoutInflater;
	private ArrayList<ThaliPojo> alAllThali;
	protected long userId;
	protected int status;
	private Holder holder;
	private ShowDialog showMessage;
	private AllThaaliFragment fragment;

	public AllThaliAdapter(Activity activity, ArrayList<ThaliPojo> alAllThali,
			AllThaaliFragment fragment) {
		super();
		this.activity = activity;
		this.alAllThali = alAllThali;
		this.fragment = fragment;
		showMessage = new ShowDialog();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (alAllThali == null) {
			return 0;
		} else {
			return alAllThali.size();
		}
	}

	@Override
	public ThaliPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alAllThali.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// R.layout.no_thaali_item
		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater
					.inflate(R.layout.all_thaali_item, null);
			holder = new Holder();
			holder.tvUserName = (TextView) convertView
					.findViewById(R.id.tv_all_thali_user_name);
			holder.tvCount = (TextView) convertView
					.findViewById(R.id.tv_all_thaali_number);

			holder.cbStatus = (CheckBox) convertView
					.findViewById(R.id.im_user_status);
			holder.imMessage = (ImageView) convertView
					.findViewById(R.id.im_msg);

			/*
			 * holder.cbStatus .setOnCheckedChangeListener(new
			 * CompoundButton.OnCheckedChangeListener() {
			 * 
			 * @Override public void onCheckedChanged(CompoundButton buttonView,
			 * boolean isChecked) { int getPosition = (Integer)
			 * buttonView.getTag(); // Here // we get the position that we have
			 * set for the checkbox using setTag.
			 * 
			 * if (isChecked) { alAllThali.get(getPosition).setStatus(1); // Set
			 * the value of // checkbox to status=1; // maintain its // state.
			 * }else { status=0; alAllThali.get(getPosition).setStatus(0); }
			 * 
			 * new UpdateUserStatusAsyncTask().execute();
			 * 
			 * } });
			 */

			/*
			 * holder.cbStatus.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * userId=alAllThali.get(position).getUserId(); holder.getPosition =
			 * (Integer) v.getTag();
			 * 
			 * if (alAllThali.get(holder.getPosition).getStatus()==1) {
			 * status=0; } else { status=1; }
			 * 
			 * new UpdateUserStatusAsyncTask().execute(); } });
			 */
			convertView.setTag(holder);
			convertView.setTag(R.id.im_user_status, holder.cbStatus);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.cbStatus.setTag(position);
		holder.tvUserName
				.setText(alAllThali.get(position).getName().toString());
		holder.tvCount.setText(alAllThali.get(position).getThaliNumber() + "");
		if (alAllThali.get(position).getStatus() == 1) {
			holder.cbStatus.setChecked(true);
		} else {
			holder.cbStatus.setChecked(false);
		}

		holder.cbStatus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				userId = alAllThali.get(position).getUserId();
				holder.getPosition = (Integer) v.getTag();
				if (alAllThali.get(holder.getPosition).getStatus() == 1) {
					status = 0;
				} else {
					status = 1;
				}

				new UpdateUserStatusAsyncTask().execute();
			}
		});

		holder.imMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				activity.startActivity(new Intent(activity,
						SendMessageActivity.class).putExtra("thali_data",
						alAllThali.get(position)));

			}
		});

		return convertView;
	}

	public static class Holder {

		TextView tvUserName;
		TextView tvCount;
		ImageView imMessage;
		CheckBox cbStatus;
		int getPosition;
	}

	class UpdateUserStatusAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", userId + ""));

			urlParameter.add(new BasicNameValuePair("status", status + ""));
			Log.v("urlParameter", urlParameter + "");

			progressDialog = new ProgressDialog(activity); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(activity,
					WebUrl.UPDATE_USER_STATUS, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						showMessage.showMsg(activity,
								jsonObject.optString("message"));
						// Toast.makeText(activity,
						// jsonObject.optString("message"),
						// Toast.LENGTH_LONG).show();
						if (fragment != null) {
							fragment.refresh();
						}
						if (alAllThali.get(holder.getPosition).getStatus() == 1) {
							alAllThali.get(holder.getPosition).setStatus(0);
							holder.cbStatus.setChecked(false);

						} else {
							alAllThali.get(holder.getPosition).setStatus(1);
							holder.cbStatus.setChecked(true);

						}

						// notifyDataSetChanged();
						// holder.cbStatus.setChecked(true);
					} else {
						holder.cbStatus.setChecked(false);
					}
				} else {

					Toast.makeText(activity, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
