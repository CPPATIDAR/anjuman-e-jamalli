package com.faizconnect.adapter;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;




import com.faizconnect.R;
import com.faizconnect.adminmodule.AddMenuDialogActivity;
import com.faizconnect.adminmodule.ChangeMenuActivity;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.pojo.ChangeMenuPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GridAdapter extends BaseAdapter{

	private Activity activity;
	private LayoutInflater layoutInflater;
	private ArrayList<ChangeMenuPojo> alChangeMenu;
	private ImageLoader imageLoader;
	private int thaliMenuId;
	private String itemId;
	private ShowDialog showMessage;
	
	public GridAdapter(Activity activity, ArrayList<ChangeMenuPojo> alChangeMenu) {
		super();
		this.activity = activity;
		this.alChangeMenu = alChangeMenu;
		imageLoader = new ImageLoader(activity);
		showMessage = new ShowDialog();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alChangeMenu.size();
	}

	@Override
	public ChangeMenuPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alChangeMenu.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		Holder holder;
		
		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.grid_single, null);
			holder = new Holder();
			holder.tvItemName = (TextView) convertView
					.findViewById(R.id.grid_text);
			holder.ivItemImage = (ImageView) convertView
					.findViewById(R.id.grid_image);
			holder.ivDelete = (ImageView) convertView
					.findViewById(R.id.iv_delete_item);
			holder.ivEdit = (ImageView) convertView.findViewById(R.id.iv_edit_item);

		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvItemName.setText(alChangeMenu.get(position).getItemName().toString());
		
		imageLoader.DisplayImage(alChangeMenu.get(position).getItemImage(),
				R.drawable.ic_launcher, holder.ivItemImage);
		
		holder.ivDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thaliMenuId = alChangeMenu.get(position).getThaliMenuId();
				itemId = alChangeMenu.get(position).getItemId();
				new AlertDialog.Builder(activity)

				.setMessage("Do you want to delete this item")
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {
								new DelateThaliItemAsyncTask().execute();
								dialog.cancel();
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						}).create().show();
				
			}
		});
		holder.ivEdit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thaliMenuId = alChangeMenu.get(position).getThaliMenuId();
				itemId = alChangeMenu.get(position).getItemId();
				
				Intent intent = new Intent(activity,
						AddMenuDialogActivity.class);
				intent.putExtra("THALI_MENU_ID", thaliMenuId+"");
				intent.putExtra("THALI_ITEM_MENU_ID", itemId+"");
				intent.putExtra("title", alChangeMenu.get(position).getItemName());
				intent.putExtra("url", alChangeMenu.get(position).getItemImage());
				activity.startActivity(intent);
				activity.finish();
				
			}
		}); 
		
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder {

		TextView tvItemName;
		ImageView ivItemImage;
		ImageView ivDelete;
		ImageView ivEdit;
	}
	
class DelateThaliItemAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		private ChangeMenuPojo pojo;
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("thaali_menu_id",thaliMenuId+""));
			urlParameter.add(new BasicNameValuePair("item_id",itemId));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(activity);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(activity, 
							WebUrl.DELETE_THALI_ITEM, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					//showMessage.showMsg(activity, jsonObject.optString("message"));
					Toast.makeText(activity, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
					activity.finish();
					/*Intent intent = new Intent(activity,ChangeMenuActivity.class);
					activity.startActivity(intent);*/
					
				} else {
					Toast.makeText(activity, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
