package com.faizconnect.adapter;

import java.util.ArrayList;

import com.faizconnect.R;
import com.faizconnect.adapter.AreaLocationAdapter.Holder;
import com.faizconnect.adminmodule.SendMessageActivity;
import com.faizconnect.pojo.NoThaaliPojo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class NoThaaliAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater layoutInflater;
	private ArrayList<NoThaaliPojo> alNoThaali;

	public NoThaaliAdapter(Activity activity, ArrayList<NoThaaliPojo> alNoThaali) {
		super();
		this.activity = activity;
		this.alNoThaali = alNoThaali;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alNoThaali.size();
	}

	@Override
	public NoThaaliPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alNoThaali.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;

		// R.layout.no_thaali_item
		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.no_thaali_item, null);
			holder = new Holder();
			holder.tvUserName = (TextView) convertView
					.findViewById(R.id.tv_no_thaali_user_name);
			holder.tvThaaliCount = (TextView) convertView
					.findViewById(R.id.tv_no_thaali_number);
			holder.tvThaaliSize = (TextView) convertView
					.findViewById(R.id.tv_lunch_box_size);
			holder.ivMessage = (ImageView) convertView
					.findViewById(R.id.iv_send_msg);

		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvUserName.setText(alNoThaali.get(position).getThaaliName().toString());
		holder.tvThaaliCount.setText(alNoThaali.get(position).getThaaliId()+"");
		holder.tvThaaliSize.setText(alNoThaali.get(position).getThaaliSize()+"");
		
		holder.ivMessage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(activity,SendMessageActivity.class);
				intent.putExtra("no_thali_data",alNoThaali.get(position));
				
//				intent.putExtra("ID", alNoThaali.get(position).getUserID());
//				intent.putExtra("Thali_ID", alNoThaali.get(position).getThaaliId());
				activity.startActivity(intent);
//				Toast.makeText(activity, alNoThaali.get(position).getThaaliId()+"", Toast.LENGTH_SHORT).show();
			}
		});
		convertView.setTag(holder);

		return convertView;
	}

	public static class Holder {

		TextView tvUserName;
		TextView tvThaaliCount;
		TextView tvThaaliSize;
		ImageView ivMessage;
	}
}
