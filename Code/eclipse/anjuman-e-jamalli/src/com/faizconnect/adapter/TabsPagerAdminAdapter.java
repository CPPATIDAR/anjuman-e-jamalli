package com.faizconnect.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.faizconnect.adminfragment.AdminProfileFragment;
import com.faizconnect.adminfragment.AllThaaliFragment;
import com.faizconnect.adminfragment.AnnouncementFragment;
import com.faizconnect.adminfragment.HomeAdminFragment;
import com.faizconnect.adminfragment.MenuAdminFragment;



public class TabsPagerAdminAdapter extends FragmentPagerAdapter {
	
	HomeAdminFragment homeAdminFragment;
	AnnouncementFragment announcementFragment;
	MenuAdminFragment menuAdminFragment;
	AllThaaliFragment allThaaliFragment;
	AdminProfileFragment adminProfileFragment;

	public TabsPagerAdminAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// UserHome fragment activity
			if(homeAdminFragment==null)
			{
				homeAdminFragment =  HomeAdminFragment.getInstance();
			}
			return homeAdminFragment;
			//return new HomeAdminFragment();
		case 1:
			// User Menu Monthly Wise fragment activity
			if(announcementFragment==null)
			{
				announcementFragment =  AnnouncementFragment.getInstance();
			}
			return announcementFragment;
			//return new AnnouncementFragment();
		case 2:
			// User Stop Thalli fragment activity
			if(menuAdminFragment==null)
			{
				menuAdminFragment =  MenuAdminFragment.getInstance();
			}
			return menuAdminFragment;
			//return new MenuAdminFragment();
		case 3:
			// USer Profile fragment activity
			if(allThaaliFragment==null)
			{
				allThaaliFragment =  AllThaaliFragment.getInstance();
			}
			return allThaaliFragment;
			//return new AllThaaliFragment();
		case 4:
			// User Contact fragment activity
			if(adminProfileFragment==null)
			{
				adminProfileFragment =  AdminProfileFragment.getInstance();
			}
			return adminProfileFragment;
			//return new UserProfileFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 5;
	}
	
	public void refresh() {
		if(menuAdminFragment!=null)
		{
			menuAdminFragment.refresh();
		}
		
		if(allThaaliFragment!=null)
		{
			allThaaliFragment.refresh();
		}
		
	}
	
	

}
