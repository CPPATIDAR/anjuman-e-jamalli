package com.faizconnect.adapter;

import java.util.ArrayList;

import com.faizconnect.R;
import com.faizconnect.pojo.AreaLocationPojo;
import com.faizconnect.pojo.UnitPojo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class UnitAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<UnitPojo> alList;
	private LayoutInflater layoutInflater;

	public UnitAdapter(Activity activity, ArrayList<UnitPojo> alList) {
		super();
		this.activity = activity;
		this.alList = alList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alList.size();
	}

	@Override
	public UnitPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return alList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;

		if (convertView == null) {
			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.cal_time_spinner,
					null);
			holder = new Holder();
			holder.tvCountry = (TextView) convertView
					.findViewById(R.id.text_spinner);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.tvCountry.setText(alList.get(position).getName().toString());

		convertView.setTag(holder);

		return convertView;
	}

	public static class Holder {

		TextView tvCountry;
	}
}
