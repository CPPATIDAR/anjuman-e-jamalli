package com.faizconnect.adminfragment;


import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminfragment.AdminJismaniFragment.FawaidAsyncTask;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AdminRuhaniFragment extends Fragment {
	
//	public UserRuhaniFragment(){}
	private String thaaliMenuId;
	private TextView tvJismani;
	private Typeface typefaceNormal;
	private ImageView imEdit;
	private String FawaidStatus="";
	private RelativeLayout rlContainer;
	private View lyAddFawaid;
	private EditText etFawaid;
	private Button btnSave;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_admin_ruhani_fawaid, container, false);
        thaaliMenuId =  getActivity().getIntent().getStringExtra("THAALI_ID");
        FawaidStatus=getActivity().getIntent().getExtras().getString("FAWAID_STATUS");
        tvJismani = (TextView) rootView.findViewById(R.id.tv_admin_ruhani_fawaid);
        imEdit = (ImageView) rootView.findViewById(R.id.im_admin_ruhani_fawaid_edit);
        rlContainer = (RelativeLayout) rootView.findViewById(R.id.rl_container);
        lyAddFawaid = (View) rootView.findViewById(R.id.ly_add_fawaid);
        etFawaid = (EditText) rootView.findViewById(R.id.et_fawaid_text);
        btnSave = (Button) rootView.findViewById(R.id.btn_save);
        
        
        
        
        if (FawaidStatus.equalsIgnoreCase("ADD FAWAID")) {
        	imEdit.setVisibility(View.VISIBLE);
     		}
        setFonts();
        
        
        imEdit.setOnClickListener(new OnClickListener() {
			
     			@Override
     			public void onClick(View v) {
     				rlContainer.setVisibility(View.GONE);
     				lyAddFawaid.setVisibility(View.VISIBLE);
     				
     				
     			}
     		});
             
             btnSave.setOnClickListener(new OnClickListener() {
     			
         			@Override
         			public void onClick(View v) {
         				rlContainer.setVisibility(View.VISIBLE);
         				lyAddFawaid.setVisibility(View.GONE);
         				if (etFawaid.getText().toString().length()!=0) {
         					new UpdateFawaidAsyncTask().execute();
						}else {
							Toast.makeText(getActivity(), "Please enter fawaid", Toast.LENGTH_LONG).show();
						}
         				
         			}
         		});
        new FawaidAsyncTask().execute();
        return rootView;
    }
	
	private void setFonts() {
		// TODO Auto-generated method stub
		
		typefaceNormal = Fonts.normalFont(getActivity());
		tvJismani .setTypeface(typefaceNormal);
	}
	
	class FawaidAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
/*			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));// 5
*/			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id", thaaliMenuId));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.ADMIN_FAWAID, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONArray jsonArray = jsonObject.optJSONArray("result");
						
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObject=jsonArray.optJSONObject(i);
							tvJismani.setText(jObject.optString("ruhani_fawaid"));
							etFawaid.setText(jObject.optString("ruhani_fawaid"));
						}
					
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	
	
class UpdateFawaidAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("ruhani_fawaid",etFawaid.getText().toString()));
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id",thaaliMenuId));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity());   //   Create Dialog
			progressDialog.setMessage("Loading...");				   //   Show message in dialog box
			progressDialog.show();	
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(), WebUrl.UpdateFawaid,
					urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response != null) {
					//
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						//JSONArray jsonArray = jsonObject.optJSONArray("result");
						Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_LONG).show();
						rlContainer.setVisibility(View.VISIBLE);
					    new FawaidAsyncTask().execute();
						

						
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
