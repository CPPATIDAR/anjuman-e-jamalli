package com.faizconnect.adminfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adapter.AllThaliAdapter;
import com.faizconnect.adminmodule.AddNewThaaliActivity;
import com.faizconnect.pojo.ThaliPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AllThaaliFragment extends Fragment {

	private ListView lvAllThali;
	private ArrayList<ThaliPojo> alAllThali;
	private ImageView ivAddThali;
	public static EditText etSearchUser;
	private SearchTranslationFilter filter;
	private static AllThaaliFragment fragment;
	private AllThaaliFragment()
	{
		
	}
	public static AllThaaliFragment getInstance(){
		if(fragment==null)
		{
			fragment = new AllThaaliFragment();
		}
		return fragment;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_all_thaali,
				container, false);

		initComponents(rootView);
		new GetAllTHaliAsyncTask().execute();

		return rootView;
	}

	private void initComponents(View rootView) {
		lvAllThali = (ListView) rootView.findViewById(R.id.listview_add_thaali);
		ivAddThali = (ImageView) rootView.findViewById(R.id.iv_add_new_thali);
		etSearchUser = (EditText) rootView.findViewById(R.id.et_search_user);

		ivAddThali.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						AddNewThaaliActivity.class);
				startActivityForResult(intent, 2902);
			}
		});
		
		etSearchUser.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (s.equals("") || s.length() == 0) {
					lvAllThali.setAdapter(new AllThaliAdapter(getActivity(),
							alAllThali,AllThaaliFragment.this));
				} else {
					getFilter().filter(s.toString());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
		etSearchUser.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				etSearchUser.setFocusable(true);
				etSearchUser.setEnabled(true);
				etSearchUser.requestFocus();
			}
		});
		
	}

	public Filter getFilter() {
		if (filter == null) {
			filter = new SearchTranslationFilter();
		}
		return filter;
	}

	private class SearchTranslationFilter extends Filter {
		private ArrayList<ThaliPojo> arrayListTemp;
		private ArrayList<ThaliPojo> search;

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			constraint = constraint.toString().toLowerCase();
			FilterResults result = new FilterResults();
			if (constraint != null && constraint.toString().length() > 0
					&& (!constraint.equals(""))) {

				arrayListTemp = new ArrayList<ThaliPojo>();

				for (int i = 0, l = alAllThali.size(); i < l; i++) {

					// System.out.println(alQuranHifzs.get(i).getUpdateDate().toString());
					// if
					// (alAllThali.get(i).getName().toString().equalsIgnoreCase(" ")||alAllThali.get(i).getName().toString().equalsIgnoreCase("")||alAllThali.get(i).getName().toString()==null)
					// {
					// pojo.setna("");
					// }else {
					if (alAllThali.get(i).getName().toString().toLowerCase()
							.contains(constraint))

						arrayListTemp.add(alAllThali.get(i));
					// }

				}

				result.count = arrayListTemp.size();
				result.values = arrayListTemp;
				// Log.v("after matching", "=" + arrayListTemp.size());
			} else {
				synchronized (this) {
					result.values = arrayListTemp;
					result.count = arrayListTemp.size();
				}
			}
			return result;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			try {
				search = (ArrayList<ThaliPojo>) results.values;
				Log.v("results.count", "=" + results.count);

				lvAllThali
						.setAdapter(new AllThaliAdapter(getActivity(), search,AllThaaliFragment.this));

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	
	// ////////////////////////////////////////////////////////////////////////

	class GetAllTHaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private ThaliPojo pojo;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("active_check", "0"));
			// urlParameter.add(new BasicNameValuePair("active_check", ));
			Log.v("urlParameter", urlParameter + "");

			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.ADD_RECIPIENTS, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						int activeIndex = 0;
						alAllThali = new ArrayList<ThaliPojo>();
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						for (int i = 0; i < jsonArray.length(); i++) {

							jsObject = jsonArray.optJSONObject(i);

							pojo = new ThaliPojo();
							pojo.setUserId(jsObject.optLong("id"));
							pojo.setName(jsObject.optString("name"));
							pojo.setStatus(jsObject.optInt("is_active"));
							pojo.setThaliNumber(jsObject
									.optInt("thaali_number"));
							pojo.setThaliSize(jsObject.optInt("thaali_size"));
							if (jsObject.optInt("is_active") == 1) {
								alAllThali.add(activeIndex++, pojo);
							} else {
								alAllThali.add(pojo);
							}
						}

						lvAllThali.setAdapter(new AllThaliAdapter(
								getActivity(), alAllThali,AllThaaliFragment.this));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void refresh() {
		new GetAllTHaliAsyncTask().execute();
	}
	

	
}
