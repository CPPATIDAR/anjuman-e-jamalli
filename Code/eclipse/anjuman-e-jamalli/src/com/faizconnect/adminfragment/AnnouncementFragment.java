package com.faizconnect.adminfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminfragment.AllThaaliFragment.GetAllTHaliAsyncTask;
import com.faizconnect.adminfragment.HomeAdminFragment.TodayThaaliAsyncTask;
import com.faizconnect.adminmodule.MakeAnnounceMentActivity;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

public class AnnouncementFragment extends Fragment {

	private PullToRefreshScrollView scrollView;
	private ScrollView mScrollView;
	private LinearLayout llParent;
	private LinearLayout llChild;
	private LayoutInflater inflater;
	public int pageCount;
	private ImageView ivSendNoti;
	private static AnnouncementFragment fragment;
	private AnnouncementFragment()
	{
		
	}
	public static AnnouncementFragment getInstance(){
		if(fragment==null)
		{
			fragment = new AnnouncementFragment();
		}
		return fragment;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_announcement,
				container, false);
		scrollView = (PullToRefreshScrollView) rootView
				.findViewById(R.id.scrollview);
		llParent = (LinearLayout) rootView
				.findViewById(R.id.ll_parent_user_announce);
		ivSendNoti = (ImageView) rootView
				.findViewById(R.id.iv_send_notification);
		pageCount = 0;
		// inflater = getActivity().getLayoutInflater();

		// ((ListView)
		// rootView.findViewById(R.id.listview_announcement)).setAdapter(new
		// VarshitAdapter(getActivity(),R.layout.announcement_item));

		new AdminAnnouncementAsyncTask().execute();

		scrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				new AdminAnnouncementAsyncTask().execute();
			}
		});

		mScrollView = scrollView.getRefreshableView();

		ivSendNoti.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						MakeAnnounceMentActivity.class);
				startActivityForResult(intent, 2903);
			}
		});

		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		// Toast.makeText(getActivity(), "Hello", Toast.LENGTH_SHORT).show();
		llParent.removeAllViews();
		pageCount = 0;
		new AdminAnnouncementAsyncTask().execute();
	}

	class AdminAnnouncementAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		// private AnnouncementPojo pojo;

		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter
					.add(new BasicNameValuePair("page_num", pageCount + ""));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.MohallaId)));

			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Loading...");
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.ADMIN_GET_ANNOUNCEMENT, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {

				JSONObject jsonObject = new JSONObject(response);

				if (jsonObject.optBoolean("status") == true) {

					JSONArray jsonArray = jsonObject.optJSONArray("result");

					for (int i = 0; i < jsonArray.length(); i++) {

						inflater = (LayoutInflater) getActivity()
								.getSystemService(
										Context.LAYOUT_INFLATER_SERVICE);
						jsObject = jsonArray.optJSONObject(i);

						llChild = (LinearLayout) inflater.inflate(
								R.layout.announcement_item, null);
						TextView tvTitle = (TextView) llChild
								.findViewById(R.id.tv_user_thali_dist);
						TextView tvDaysAgo = (TextView) llChild
								.findViewById(R.id.tv_user_thali_days_ago);
						TextView tvDiscription = (TextView) llChild
								.findViewById(R.id.tv_user_ann_desc);
						TextView tvDate = (TextView) llChild
								.findViewById(R.id.tv_announce_date);

						/*
						 * tvTitle.setTypeface(typefaceBold);
						 * tvDiscription.setTypeface(typefaceNormal);
						 * tvDaysAgo.setTypeface(typefaceNormal);
						 */

						String[] dateTime = jsObject.optString("date").split(
								" ");
						String engDate[] = dateTime[0].split("-");
						int month = Integer.parseInt(engDate[1]) - 1;
						String hijriDate[] = jsObject.optString("hijari_date")
								.split("-");

						// Log.v("month", month+"");

						tvDate.setText(hijriDate[2] + " "
								+ jsObject.optString("hijari_month") + " / "
								+ engDate[2] + " " + getEnglishIndex(month)
								+ ", " + dateTime[1]);

						tvTitle.setText(jsObject.optString("title"));
						tvDiscription.setText(jsObject
								.optString("announcement"));
						tvDaysAgo.setText(jsObject.optString("daysago"));

						llParent.addView(llChild);

					}
					pageCount++;
					scrollView.onRefreshComplete();

				} else {
					Toast.makeText(getActivity(),
							jsonObject.optString("message"), Toast.LENGTH_SHORT)
							.show();
					scrollView.onRefreshComplete();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public String getEnglishIndex(int month) {
		String val = "";
		String[] iMonthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sept", "Oct", "Nov", "Dec" };

		val = iMonthNames[month];

		return val;
	}

}
