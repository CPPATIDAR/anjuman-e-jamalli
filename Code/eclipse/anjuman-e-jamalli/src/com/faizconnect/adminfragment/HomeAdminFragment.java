package com.faizconnect.adminfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminmodule.AddNewThaaliActivity;
import com.faizconnect.adminmodule.AdminDashboardActivity;
import com.faizconnect.adminmodule.MakeAnnounceMentActivity;
import com.faizconnect.adminmodule.NoThaaliTodayActivity;
import com.faizconnect.adminmodule.PublicFeedbackActivity;
import com.faizconnect.adminmodule.SendMessageActivity;
import com.faizconnect.common.AdminThaliMenuView;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class HomeAdminFragment extends Fragment {

	private LinearLayout llNoThaliToday;
	private LinearLayout llPublicFeedback;
	private LinearLayout llAnnouncement;
	private LinearLayout llSendMessage;
	private LinearLayout llMonthlyView;
	private LinearLayout llAddThali;
	private LinearLayout llThaliLayout;
	private TextView tvNoThaliCount;
	private TextView tvPublicFeed;
	private static HomeAdminFragment homeAdminFragment;
	private HomeAdminFragment()
	{
		
	}
	public static HomeAdminFragment getInstance(){
		if(homeAdminFragment==null)
		{
			homeAdminFragment = new HomeAdminFragment();
		}
		return homeAdminFragment;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home_admin, container, false);
		
		llNoThaliToday = (LinearLayout) rootView.findViewById(R.id.ll_no_thaali);
		llPublicFeedback = (LinearLayout) rootView.findViewById(R.id.ll_public_feebback);
		llAnnouncement = (LinearLayout) rootView.findViewById(R.id.ll_make_announce);
		llSendMessage = (LinearLayout) rootView.findViewById(R.id.ll_send_message);
		llMonthlyView = (LinearLayout) rootView.findViewById(R.id.ll_monthly_view);
		llAddThali = (LinearLayout) rootView.findViewById(R.id.ll_add_new_thali);
		tvNoThaliCount = (TextView) rootView.findViewById(R.id.tv_no_thali_noti);
		tvPublicFeed = (TextView) rootView.findViewById(R.id.tv_pub_feedback_noti);
		
		llThaliLayout = (LinearLayout) rootView.findViewById(R.id.ll_admin_table_layout);
		
		llNoThaliToday.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),NoThaaliTodayActivity.class);
				startActivity(intent);
			}
		});
		
		llPublicFeedback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tvPublicFeed.setText("0");
				Intent intent = new Intent(getActivity(),PublicFeedbackActivity.class);
				intent.putExtra("FeedBack_Status", "Yesterday");
				startActivity(intent);
			}
		});
		
		llAnnouncement.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),MakeAnnounceMentActivity.class);
				startActivity(intent);
			}
		});
		
		llSendMessage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),SendMessageActivity.class);
				startActivity(intent);
			}
		});
		
		llMonthlyView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((AdminDashboardActivity) getActivity()).setMenuScreen();
			}
		});
		
		llAddThali.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),AddNewThaaliActivity.class);
				//startActivityForResult(intent,2902);
				startActivity(intent);
			}
		});
		
		new TodayThaaliAsyncTask().execute();
		//new StopThaliCountAsyncTask().execute();
		
		return rootView;
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(getActivity()instanceof AdminDashboardActivity)
		{
			((AdminDashboardActivity)getActivity()).setAllThaliScreen();
		}
		
		
	}
	class TodayThaaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(),SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.HOME_TODAY_THAALI, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					//llThaliLayout.removeAllViews(); 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
						
						llThaliLayout.addView(AdminThaliMenuView.inflateMenu(getActivity(), jsonObject2, false, true,
								R.drawable.home_menu_button_selector,R.color.home_menu_button_font,false,false));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	class StopThaliCountAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(),SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.NO_THAALI_COUNT, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					//llThaliLayout.removeAllViews(); 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						jsObject= jsonObject.optJSONObject("result");
						tvNoThaliCount.setText(jsObject.optString("stop_thaali_count"));
						tvPublicFeed.setText(jsObject.optString("feedback_count"));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void updateCount(String nothalli,String feedCount)
	{
		if(tvNoThaliCount!=null)
		{
			tvNoThaliCount.setText(nothalli);	
		}
		if(tvPublicFeed!=null)
		{
			tvPublicFeed.setText(feedCount);	
		}
	}
	

}
