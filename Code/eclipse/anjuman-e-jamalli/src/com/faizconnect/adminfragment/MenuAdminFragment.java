package com.faizconnect.adminfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.common.AdminMonthlyThaliMenuView;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class MenuAdminFragment extends Fragment {

	
	private int hijriMonth;
	private int hijriYear;
	private int tmpHijriMonth;
	private int tmpHijriYear;

	private TextView tvHijriMonth;
	private ImageView ivRightArrow;
	private ImageView ivLeftArrow;
	private TextView tvEnglishMonth;
	public int monthStart;
	public int monthEnd;
	private Typeface typefaceNormal;
	private ShowDialog showMessage;
	// protected boolean lastHijrtMonthData;
	private LinearLayout llMenus;

	private static MenuAdminFragment fragment;
	private MenuAdminFragment()
	{
		
	}
	public static MenuAdminFragment getInstance(){
		if(fragment==null)
		{
			fragment = new MenuAdminFragment();
		}
		return fragment;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_monthly_view,
				container, false);

		initComponent(rootView);
		return rootView;
	}

	private void initComponent(View rootView) {
		tmpHijriMonth = hijriMonth = SPAnjuman.getIntValue(getActivity(),
				SPAnjuman.HijriMonth);
		tmpHijriYear = hijriYear = SPAnjuman.getIntValue(getActivity(),
				SPAnjuman.HijriYear);
		tvHijriMonth = (TextView) rootView
				.findViewById(R.id.tv_admin_monthly_hijri_month);
		tvEnglishMonth = (TextView) rootView
				.findViewById(R.id.tv_admin_monthly_english_month);
		ivLeftArrow = (ImageView) rootView.findViewById(R.id.iv_left_arrow);
		ivRightArrow = (ImageView) rootView.findViewById(R.id.iv_right_arrow);
		// lvMenus = (ListView)
		// rootView.findViewById(R.id.listview_menu_monthly);
		llMenus = (LinearLayout) rootView
				.findViewById(R.id.ll_menu_admin_monthly);

		showMessage = new ShowDialog();
		setFonts();

		new GetThaliMenuAdminAsyncTask().execute();

		ivLeftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tmpHijriMonth = hijriMonth ;
				tmpHijriYear = hijriYear ;
				if (tmpHijriMonth == 1) {
					tmpHijriMonth = 12;
					tmpHijriYear--;
				} else {
					tmpHijriMonth--;
				}
				new GetThaliMenuAdminAsyncTask().execute();

			}
		});

		ivRightArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tmpHijriMonth = hijriMonth ;
				tmpHijriYear = hijriYear ;
				if (tmpHijriMonth == 12) {
					tmpHijriMonth = 1;
					tmpHijriYear++;
				} else {
					tmpHijriMonth++;
				}
				new GetThaliMenuAdminAsyncTask().execute();
			}
		});

	}

	class GetThaliMenuAdminAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("hijari_year", tmpHijriYear
					+ ""));
			urlParameter.add(new BasicNameValuePair("hijari_month", tmpHijriMonth
					+ ""));

			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.GET_THALI_MENU_ITEM_ADMIN, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")) {
						hijriMonth =  tmpHijriMonth ;
						hijriYear  =  tmpHijriYear ;
						JSONArray jArray = jsonObject.optJSONArray("result");

						// for (int i = 0; i < jArray.length(); i++) {

						// if (i==0) {
						jsObject = jArray.optJSONObject(0);
						tvHijriMonth
								.setText(jsObject.optString("hijari_month"));
						// tvPrice.setText(jsObject.optString("expense"));
						String engDate[] = jsObject.optString("date")
								.split("-");
						Log.v("Date==", jsObject.optString("date") + "");
						monthStart = Integer.parseInt(engDate[1]) - 1;
						Log.v("monthStart", monthStart + "");
						// tvEnglishMonth
						// }
						JSONObject jsObject1 = jArray.optJSONObject(jArray
								.length() - 1);

						String engLastDate[] = jsObject1.optString("date")
								.split("-");
						monthEnd = Integer.parseInt(engLastDate[1]) - 1;
						Log.v("monthEnd", monthEnd + "");
						if (monthStart == monthEnd) {
							tvEnglishMonth.setText(getEnglishIndex(monthStart));
						} else {
							tvEnglishMonth.setText(getEnglishIndex(monthStart)
									+ "-" + getEnglishIndex(monthEnd));
						}
						// }

						if (jArray != null) {
							if (jArray.length() > 0) {
								llMenus.removeAllViews();
								for (int i = 0; i < jArray.length(); i++) {
									JSONObject jobj = jArray.optJSONObject(i);
									llMenus.addView(AdminMonthlyThaliMenuView.inflateMenu(
											getActivity(),
											jArray.optJSONObject(i), true,
											true,
											R.drawable.menu_button_selector,
											R.color.menu_button_font, true,
											true,
											jobj.optBoolean("show_feedback")));
									TextView tv = new TextView(getActivity());
									tv.setText(".");
									tv.setVisibility(View.INVISIBLE);
									llMenus.addView(tv);
								}

								// lvMenus.setAdapter(new AdminMenuThaliAdapter(
								// getActivity(), jArray));
							}
						}
					} else {
						
						showMessage.showMsg(getActivity(),
								jsonObject.optString("message"));
						// Toast.makeText(getActivity(),
						// jsonObject.optString("message"),
						// Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getEnglishIndex(int month) {
		String val = "";
		String[] iMonthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sept", "Oct", "Nov", "Dec" };

		val = iMonthNames[month];

		return val;
	}

	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceNormal = Fonts.normalFont(getActivity());
		tvHijriMonth.setTypeface(typefaceNormal);
		tvEnglishMonth.setTypeface(typefaceNormal);
	}

	public void refresh() {
		new GetThaliMenuAdminAsyncTask().execute();
	}
}
