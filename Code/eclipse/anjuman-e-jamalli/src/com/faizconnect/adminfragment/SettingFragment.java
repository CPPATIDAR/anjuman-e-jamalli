package com.faizconnect.adminfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminfragment.AnnouncementFragment.AdminAnnouncementAsyncTask;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class SettingFragment extends Fragment {

	private EditText etEmail;
	private EditText etPhone;
	private EditText etPassword;
	private TextView tvNameText;
	private TextView tvName;
	private TextView tvEmailText;
	private TextView tvPhoneText;
	private TextView tvCountryText;
	private TextView tvCountry;
	private TextView tvStateText;
	private TextView tvState;
	private TextView tvCityText;
	private TextView tvCity;
	private TextView tvMohallahText;
	private TextView tvMohallah;
	private TextView tvSabeelNumberText;
	private TextView tvSabeelNumber;
	private TextView tvThaliNumberText;
	private TextView tvThaliNumber;
	private TextView tvThaliSizeText;
	private TextView tvThaliSize;
	private TextView tvPasswordText;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_setting, container, false);
		initComponents(rootView);
		
		return rootView;
	}


	private void initComponents(View rootView) {
		// TODO Auto-generated method stub
		tvName = (TextView) rootView.findViewById(R.id.tv_setting_username);
		tvNameText = (TextView) rootView.findViewById(R.id.tv_setting_username_text);
		etEmail =  (EditText) rootView.findViewById(R.id.et_setting_mail);
		tvEmailText = (TextView) rootView.findViewById(R.id.tv_setting_mail);
		etPhone =  (EditText) rootView.findViewById(R.id.et_setting_phone);
		tvPhoneText = (TextView) rootView.findViewById(R.id.tv_setting_phone);
		tvCountry = (TextView) rootView.findViewById(R.id.tv_setting_country);
		tvCountryText = (TextView) rootView.findViewById(R.id.tv_setting_country_text);
		tvState = (TextView) rootView.findViewById(R.id.tv_setting_state);
		tvStateText = (TextView) rootView.findViewById(R.id.tv_setting_state_text);
		tvCity = (TextView) rootView.findViewById(R.id.tv_setting_city);
		tvCityText = (TextView) rootView.findViewById(R.id.tv_setting_city_text);
		tvMohallah = (TextView) rootView.findViewById(R.id.tv_setting_mohalla);
		tvMohallahText = (TextView) rootView.findViewById(R.id.tv_setting_mohalla_text);
		tvSabeelNumber = (TextView) rootView.findViewById(R.id.tv_setting_sabeel_number);
		tvSabeelNumberText = (TextView) rootView.findViewById(R.id.tv_setting_sabeel_number_text);
		tvThaliNumber = (TextView) rootView.findViewById(R.id.tv_setting_thali_number);
		tvThaliNumberText = (TextView) rootView.findViewById(R.id.tv_setting_thali_number_text);
		tvThaliSize = (TextView) rootView.findViewById(R.id.tv_setting_thali_size);
		tvThaliSizeText = (TextView) rootView.findViewById(R.id.tv_setting_thali_size_text);
		tvPasswordText = (TextView) rootView.findViewById(R.id.tv_setting_password);
		etPassword =  (EditText) rootView.findViewById(R.id.et_setting_password);
	
		etEmail.setEnabled(false);
		etPhone.setEnabled(false);
		etPassword.setEnabled(false);

		new UserProfileAsyncTask().execute();
	}
	
	class UserProfileAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
//		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));// 5
			Log.v("urlParameter", urlParameter + "");
//			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
//			progressDialog.setMessage("Loading..."); // Show message in dialog
//														// box
//			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
//			response = CustomHttpClient.executeHttpPost(getActivity(),
//					WebUrl.ADMIN_SETTING, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						jsObject = jsonObject.optJSONObject("result");

						/*tvUsername.setText(jsObject.optString("fullname"));
						etEmail.setText(jsObject.optString("email"));
						etPhone.setText(jsObject.optString("phone"));
						tvSabeelNumber.setText(jsObject
								.optString("sabeel_number"));
						tvThaliNo.setText(jsObject.optString("thaali_number"));
						tvThaliSize.setText(jsObject.optString("thaali_size"));
						etPassword.setText(jsObject.optString("password"));
						if (jsObject.optString("image").equalsIgnoreCase("")
								|| jsObject.optString("image").equalsIgnoreCase(null)) {
							
						} else {
							imageLoader.DisplayImage(jsObject.optString("image"), R.drawable.ic_launcher, ivUserPic);
						}*/
						
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	


}
