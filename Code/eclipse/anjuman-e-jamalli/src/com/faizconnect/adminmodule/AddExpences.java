package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.LoginActivity;
import com.faizconnect.R;
import com.faizconnect.adapter.AddExpensesAdapter;
import com.faizconnect.adapter.UnitAdapter;
import com.faizconnect.pojo.ExpensesPojo;
import com.faizconnect.pojo.UnitPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AddExpences extends Activity {
	private String thaaliMenuId;
	private ScrollView scrollView;
	private LinearLayout llScroll;
	private TextView tvTotal;
	private TextView tvAddMoreItem;
	public ArrayList<ExpensesPojo> alExpences = new ArrayList<ExpensesPojo>();;
	public ImageView imEdit;
	public ImageView imDelete;
	public EditText etTitle;
	public EditText etPrice;
	public EditText etQuantity;
	private ListView lvAddExpenses;
	private Button btnSaveExpenses;
	protected JSONArray jsonArray;
	private ImageView imBackArrow;
	private TextView tvHeaderText;
	private LinearLayout llHeaderotification;
	private String dialogTitle = "";
	private double dialogPrice = 0;
	private double dialogQuantity = 0;
	public ArrayList<UnitPojo> alUnit = new ArrayList<UnitPojo>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_expenses);

		initComponent();
		initListners();
		
		new GetUnitAsyncTask().execute();
	}

	private void initListners() {
		imBackArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		tvAddMoreItem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				addUpdateItem();
			}
		});

	}

	protected void addUpdateItem() {
		final Dialog dialog = new Dialog(AddExpences.this);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.add_expenses_dialog);
		final EditText etTitle = (EditText) dialog
				.findViewById(R.id.et_expence_title);
		final EditText etPrice = (EditText) dialog
				.findViewById(R.id.et_expence_price);
		((TextView) dialog.findViewById(R.id.tv_expences_currency)).setText(SPAnjuman.getValue(AddExpences.this, SPAnjuman.CURRENCY));
		final EditText etQuantity = (EditText) dialog
				.findViewById(R.id.et_expence_quantity);
		final TextView tvUnit = (TextView) dialog
				.findViewById(R.id.tv_expence_unit);
		tvUnit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setUnit((TextView) v);
			}

		});

		Button btnOk = (Button) dialog
				.findViewById(R.id.btn_ok_expenses);

		etTitle.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() > 0) {
					dialogTitle = s.toString();
				}
			}
		});

		etPrice.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

				// alExpences.get(holder.pos).setPrice(s.toString());
				if (s.length() > 0) {
					dialogPrice = Double.parseDouble(s.toString());
				}
			}
		});

		etQuantity.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

				// alExpences.get(holder.pos).setQuantity(s.toString());
				if (s.length() > 0) {
					dialogQuantity = Double.parseDouble(s.toString());
				}

			}
		});

		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (dialogTitle.length() != 0 && dialogPrice != 0.0
						&& dialogQuantity != 0&&tvUnit.getText().toString().length()>0) {
					ExpensesPojo expensesPojo = new ExpensesPojo();
					expensesPojo.setTitle(dialogTitle);
					expensesPojo.setPrice(dialogPrice);
					expensesPojo.setQuantity(dialogQuantity);
					expensesPojo.setUnit(tvUnit.getText().toString());
					expensesPojo.setUnitId(((UnitPojo)tvUnit.getTag()).getId());
					alExpences.add(expensesPojo);
					// lvAddExpenses.removeAllViews();
					lvAddExpenses
							.setAdapter(new AddExpensesAdapter(
									AddExpences.this, alExpences,
									thaaliMenuId,alUnit));
					double total = 0;
					for (int i = 0; i < alExpences.size(); i++) {
						total += alExpences.get(i).getPrice();
					}
					tvTotal.setText(total + "");

					dialog.dismiss();
				} else {
					Toast.makeText(AddExpences.this,
							"Please fill Item name,Price and Quantity and Unit",
							Toast.LENGTH_LONG).show();
				}

			}
		});
		dialog.show();

	}

	private void setUnit(final TextView v) {

		AlertDialog.Builder myDialog = new AlertDialog.Builder(AddExpences.this);

		// myDialog.set
		final ListView listview = new ListView(AddExpences.this);
		LinearLayout layout = new LinearLayout(AddExpences.this);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.addView(listview);
		myDialog.setView(layout);
		UnitAdapter arrayAdapter = new UnitAdapter(AddExpences.this, alUnit);
		listview.setAdapter(arrayAdapter);

		final AlertDialog myalertDialog = myDialog.show();
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				v.setText(alUnit.get(position).getName());
				v.setTag(alUnit.get(position));
				myalertDialog.dismiss();
			}
		});

	}

	private void initComponent() {
		thaaliMenuId = getIntent().getStringExtra("THAALI_ID");
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		imBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		lvAddExpenses = (ListView) findViewById(R.id.lv_add_expenses);
		llHeaderotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		lvAddExpenses = (ListView) findViewById(R.id.lv_add_expenses);

		tvTotal = (TextView) findViewById(R.id.tv_total);
		tvAddMoreItem = (TextView) findViewById(R.id.tv_add_more_item);
		btnSaveExpenses = (Button) findViewById(R.id.btn_save_expenses);
		tvHeaderText.setText("Add Expenses");
		llHeaderotification.setVisibility(View.GONE);
		((TextView) findViewById(R.id.tv_add_expences_currency)).setText(SPAnjuman.getValue(AddExpences.this, SPAnjuman.CURRENCY));
		
	}

	class GetExpencesAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id",
					thaaliMenuId));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(AddExpences.this); // Create
																	// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(AddExpences.this,
					WebUrl.GET_EXPENSES, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			super.onPostExecute(result);
			try {
				if (response != null) {
					//
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");

						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObject = jsonArray.optJSONObject(i);
							ExpensesPojo pojo = new ExpensesPojo();
							pojo.setTitle(jObject.optString("title"));
							pojo.setPrice(jObject.optDouble("price"));
							pojo.setQuantity(jObject.optDouble("quantity"));
							pojo.setUnit(jObject.optString("unit"));
							pojo.setUnitId(jObject.optLong("unit_id"));
							alExpences.add(pojo);

							lvAddExpenses
									.setAdapter(new AddExpensesAdapter(
											AddExpences.this, alExpences,
											thaaliMenuId,alUnit));

							double total = 0;
							for (int j = 0; j < alExpences.size(); j++) {
								total += alExpences.get(j).getPrice();
							}
							tvTotal.setText(total + "");
						}

					}
				} else {
					Toast.makeText(AddExpences.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class GetUnitAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			progressDialog = new ProgressDialog(AddExpences.this); // Create
																	// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			response = CustomHttpClient.executeHttpPost(AddExpences.this,
					WebUrl.GET_UNIT, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			super.onPostExecute(result);
			new GetExpencesAsyncTask().execute();
			try {
				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alUnit = new ArrayList<UnitPojo>();
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObject = jsonArray.optJSONObject(i);
							UnitPojo pojo = new UnitPojo();
							pojo.setId(jObject.optLong("unit_id"));
							pojo.setName(jObject.optString("title"));
							alUnit.add(pojo);
						}
					}
				} else {
					Toast.makeText(AddExpences.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class AddExpensesAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id",
					thaaliMenuId));
			urlParameter.add(new BasicNameValuePair("data", jsonArray + ""));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(AddExpences.this); // Create
																	// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(AddExpences.this,
					WebUrl.ADD_EXPENSES, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			super.onPostExecute(result);
			try {
				if (response != null) {
					//
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");

					}
				} else {
					Toast.makeText(AddExpences.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
