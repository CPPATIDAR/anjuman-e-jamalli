package com.faizconnect.adminmodule;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.utill.MultipartUtility;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class AddMenuDialogActivity extends Activity {

	private EditText etTitle;
	private ImageView ivItemImge;
	private Button btnSave;
	private ShowDialog showMessage;
	public static final String IMAGE_DIRECTORY_NAME = "Anjuman-E-Jamali";
	public static final int MEDIA_TYPE_IMAGE = 2;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private Uri fileUri;
	private String selectedImagePath = "";
	private String thaliMenuId;
	private String itemId;
	private RelativeLayout rlMenuItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_add_change_menu);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		etTitle = (EditText) findViewById(R.id.et_change_menu);
		btnSave = (Button) findViewById(R.id.btn_change_menu_save);
		ivItemImge = (ImageView) findViewById(R.id.iv_add_item_image);
		Intent intent = getIntent();
		thaliMenuId = intent.getStringExtra("THALI_MENU_ID");
		itemId = intent.getStringExtra("THALI_ITEM_MENU_ID");
		if (itemId != null) {
			if (itemId.trim().length() > 0) {
				etTitle.setText(intent.getStringExtra("title"));
				new ImageLoader(AddMenuDialogActivity.this).DisplayImage(
						intent.getStringExtra("url"), R.drawable.ic_launcher,
						ivItemImge);
				btnSave.setText("Update Menu");
			}
		}
		rlMenuItem = (RelativeLayout) findViewById(R.id.rl_change_menu_image_item);
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etTitle.getText().toString().length() == 0) {
					showMessage.showMsg(AddMenuDialogActivity.this,
							"Please Enter Menu Name");
				} else if (selectedImagePath.length() == 0 && itemId == null) {
					showMessage.showMsg(AddMenuDialogActivity.this,
							"Please Select Image");
				} else {
					new UpdateChangeMenuAsyncTask().execute();
				}
			}
		});

		rlMenuItem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CharSequence colors[] = new CharSequence[] { "Gallery",
						"Camera" };

				AlertDialog.Builder builder = new AlertDialog.Builder(
						AddMenuDialogActivity.this);
				builder.setTitle("Upload photo from");
				builder.setItems(colors, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							selectImage();
							break;
						case 1:
							captureImage();
							break;
						default:
							break;
						}
					}
				});
				builder.show();

			}
		});
	}

	public void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		try {
			intent.putExtra("return-data", true);
			startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void selectImage() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_PICK);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		try {
			intent.putExtra("return-data", true);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {
		Log.e("before directory", "yes");
		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			Log.e("storage directory", "yes");
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {

				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 8;

				final Bitmap bitmap = BitmapFactory.decodeFile(
						fileUri.getPath(), options);

				ivItemImge.setImageBitmap(bitmap);
				selectedImagePath = fileUri.getPath();

				// //

			} else if (resultCode == Activity.RESULT_CANCELED) {

			} else {
				// failed to capture image
				Toast.makeText(AddMenuDialogActivity.this,
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} else if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				try {

					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					ivItemImge.setImageBitmap(BitmapFactory
							.decodeFile(selectedImagePath));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	class UpdateChangeMenuAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ProgressDialog progressDialog;
		MultipartUtility mu = null;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(AddMenuDialogActivity.this); // Create
																				// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show(); // box
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {
				if (itemId != null) {
					if (itemId.trim().length() > 0) {
						mu = new MultipartUtility(WebUrl.UPDATE_THALI_ITEM,
								"UTF-8");
						mu.addFormField("tbl_thaali_menu_items_id", itemId);
					}
				}
				if (mu == null) {
					mu = new MultipartUtility(WebUrl.ADD_THALI_ITEM, "UTF-8");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			mu.addFormField("title", etTitle.getText().toString());
			mu.addFormField("thaali_menu_id", thaliMenuId);
			mu.addFormField("device_type", "android");

			if (selectedImagePath != null) {
				if (selectedImagePath.trim().length() > 0) {
					try {
						mu.addFilePart("uploaded_file", selectedImagePath);
						mu.addFormField("user_picture_status", "true");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			try {
				response = mu.finish();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.e("responseRegistrationRequest", response);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						Toast.makeText(AddMenuDialogActivity.this,
								jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						finish();
					}
				} else {
					Toast.makeText(AddMenuDialogActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
