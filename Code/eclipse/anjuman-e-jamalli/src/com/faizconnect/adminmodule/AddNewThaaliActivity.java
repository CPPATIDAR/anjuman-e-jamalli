package com.faizconnect.adminmodule;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.utill.EmailValidator;
import com.faizconnect.utill.MultipartUtility;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class AddNewThaaliActivity extends Activity{

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private Button btnSave;
	private EditText etName;
	private EditText etEmail;
	private EditText etEjamaatId;
	private EditText etPhone;
	private EditText etSabeelNo;
	private EditText etThaliNo;
	private EditText etThaliSize;
	private ImageView ivUser;
	
	public static final String IMAGE_DIRECTORY_NAME = "Anjuman-E-Jamali";
	public static final int MEDIA_TYPE_IMAGE = 2;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private Uri fileUri;
	private String selectedImagePath;
	private ShowDialog showMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new_thaali);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Add New Thaali");
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		btnSave = (Button) findViewById(R.id.btn_Add_new_thaali_save);
		etName = (EditText) findViewById(R.id.et_Add_new_thaali_username);
		etEmail = (EditText) findViewById(R.id.et_Add_new_thaali_email);
		etEjamaatId = (EditText) findViewById(R.id.et_Add_new_thaali_e_jamaat_id);
		etPhone = (EditText) findViewById(R.id.et_Add_new_thaali_phone);
		etSabeelNo = (EditText) findViewById(R.id.et_add_new_sabeel_number);
		etThaliNo = (EditText) findViewById(R.id.et_Add_new_thaali_thaali_number);
		etThaliSize = (EditText) findViewById(R.id.et_Add_new_thaali_thaali_size);
		ivUser =  (ImageView) findViewById(R.id.iv_add_new_user_pic);
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AddNewThaaliActivity.this,NotificatonActivity.class);
				startActivity(intent);
			}
		});
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		ivUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CharSequence colors[] = new CharSequence[] { "Gallery",
						"Camera" };

				AlertDialog.Builder builder = new AlertDialog.Builder(
						AddNewThaaliActivity.this);
				builder.setTitle("Upload photo from");
				builder.setItems(colors, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							selectImage();
							break;
						case 1:
							captureImage();
							break;
						default:
							break;
						}
					}
				});
				builder.show();

			}
		});

		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(AddNewThaaliActivity.this,HomeActivity.class);
//				startActivity(intent);
//				finish();
				if (etName.getText().toString().length()==0) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Name.");
				}else if (etEmail.getText().toString().length()!=0&&!EmailValidator.isValid(etEmail.getText().toString())) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Valid E-mail.");
				}else if (etEjamaatId.getText().toString().length()==0) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter HOF E-Jamaat Id.");
				}else if (etEjamaatId.getText().toString().length()!=8) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Valid HOF E-Jamaat Id.");
				}else if (etSabeelNo.getText().toString().length()==0) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Sabeel Number.");
				}else if (etThaliNo.getText().toString().length()==0) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Thali Number.");
				}else if (etThaliSize.getText().toString().length()==0) {
					showMessage.showMsg(AddNewThaaliActivity.this, "Please Enter Thali Size.");
				} else {
					new AddNewThaliAsyncTask().execute();
				}
				
				
			}
		});
		
	}
	
	
	public void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		try {
			intent.putExtra("return-data", true);
			startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void selectImage() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_PICK);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		try {
			intent.putExtra("return-data", true);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {
		Log.e("before directory", "yes");
		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			Log.e("storage directory", "yes");
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {

				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 8;

				final Bitmap bitmap = BitmapFactory.decodeFile(
						fileUri.getPath(), options);

				ivUser.setImageBitmap(bitmap);
				selectedImagePath = fileUri.getPath();

				// //

			} else if (resultCode == Activity.RESULT_CANCELED) {

			} else {
				// failed to capture image
				Toast.makeText(AddNewThaaliActivity.this, "Sorry! Failed to capture image",
						Toast.LENGTH_SHORT).show();
			}
		} else if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				try {

					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					ivUser.setImageBitmap(BitmapFactory
							.decodeFile(selectedImagePath));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null,
				null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}
	
	class AddNewThaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
//		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		MultipartUtility mu;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//urlParameter = new ArrayList<NameValuePair>();
			progressDialog = new ProgressDialog(AddNewThaaliActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {

				mu = new MultipartUtility(WebUrl.ADD_NEW_THALI, "UTF-8");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			mu.addFormField("mohalla_id",
					SPAnjuman.getValue(AddNewThaaliActivity.this, SPAnjuman.MohallaId));
//			mu.addFormField("ejamaat_id",
//					SPAnjuman.getValue(AddNewThaaliActivity.this, SPAnjuman.EJamaatId));
			mu.addFormField("name", etName.getText().toString());
			mu.addFormField("email", etEmail.getText().toString());
			mu.addFormField("phone", etPhone.getText().toString());
			mu.addFormField("sabeel_number", etSabeelNo.getText().toString());
			mu.addFormField("thaali_number", etThaliNo.getText().toString());
			mu.addFormField("thaali_size", etThaliSize.getText().toString());
			mu.addFormField("ejamaat_id", etEjamaatId.getText().toString());
			mu.addFormField("device_type", "android");
			
			System.out.println();
			
			if (selectedImagePath != null) {
				try {
					mu.addFilePart("uploaded_file", selectedImagePath);
					mu.addFormField("user_picture_status", "true");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try {
				response = mu.finish();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.e("responseRegistrationRequest", response);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						Toast.makeText(AddNewThaaliActivity.this,
								jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						finish();
						//((AdminDashboardActivity) getActivity()).setMenuScreen();
						
						//new AdminDashboardActivity().setAllThaliScreen();
						
//						startActivity(getIntent());
					}else{
						showMessage.showMsg(AddNewThaaliActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(AddNewThaaliActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	
	
	
}