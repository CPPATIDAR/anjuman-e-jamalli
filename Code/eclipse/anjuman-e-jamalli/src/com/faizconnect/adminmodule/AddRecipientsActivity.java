package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adapter.AddRecipientsAdapter;
import com.faizconnect.pojo.ThaliPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AddRecipientsActivity extends Activity {

	private ImageView ivBackArrow;
	private ListView listView;
	private ArrayList<ThaliPojo> alRecipients;
	private AddRecipientsAdapter adapter;
	private TextView tvUserDone;
	private TextView tvHeaderText;
	private long tmpId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_recipients);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvUserDone = (TextView) findViewById(R.id.tv_select_user_done);
		listView = (ListView) findViewById(R.id.listview_add_recipients);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		
		Intent data = getIntent();
		if (data != null) {
			if (data.getSerializableExtra("SelectUser") != null) {
				alRecipients = (ArrayList<ThaliPojo>) data
						.getSerializableExtra("SelectUser");
			}
			tmpId = data.getLongExtra("single_selection", 0);
		}
		if(alRecipients!=null)
		{
			adapter = new AddRecipientsAdapter(
					AddRecipientsActivity.this, alRecipients);
			listView.setAdapter(adapter);
		}else{
			alRecipients = new ArrayList<ThaliPojo>();
		new AddRecipientsAsyncTask().execute();
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub

		tvUserDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(alRecipients.size()==0)
				{Toast.makeText(AddRecipientsActivity.this, "Please Select atleast one recipient", Toast.LENGTH_LONG).show();}else{
				Intent intent = new Intent();
				intent.putExtra("SelectUser", alRecipients);
				setResult(50, intent);
				finish();
				}

			}
		});

		ivBackArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		

	}

	public void setData(ArrayList<ThaliPojo> alSelect) {

		Intent intent = new Intent();
		intent.putExtra("SelectUser", alSelect);
		setResult(50, intent);
		finish();
	}

	class AddRecipientsAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private ThaliPojo pojo;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(AddRecipientsActivity.this, SPAnjuman.UserId)));
			urlParameter
					.add(new BasicNameValuePair("mohalla_id", SPAnjuman
							.getValue(AddRecipientsActivity.this,
									SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("active_check", "1"));
			// urlParameter.add(new BasicNameValuePair("active_check", ));
			Log.v("urlParameter", urlParameter + "");

			progressDialog = new ProgressDialog(AddRecipientsActivity.this); // Create
																				// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					AddRecipientsActivity.this, WebUrl.ADD_RECIPIENTS,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONArray jsonArray = jsonObject.optJSONArray("result");
						for (int i = 0; i < jsonArray.length(); i++) {

							jsObject = jsonArray.optJSONObject(i);

							pojo = new ThaliPojo();
							pojo.setUserId(jsObject.optLong("id"));
							pojo.setName(jsObject.optString("name"));
							pojo.setThaliNumber(jsObject.optLong("thaali_number"));
							pojo.setThaliSize(jsObject.optInt("thaali_size"));
							if(tmpId==jsObject.optLong("id"))
							{
								pojo.setStatus(1);
							}
							alRecipients.add(pojo);
							// 	No need to get is_active in pojo - It can create a big issue on code
						}
						adapter = new AddRecipientsAdapter(
								AddRecipientsActivity.this, alRecipients);
						listView.setAdapter(adapter);
					}
				} else {
					Toast.makeText(AddRecipientsActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


}
