package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adapter.TabsPagerAdminAdapter;
import com.faizconnect.adminfragment.AllThaaliFragment;
import com.faizconnect.adminfragment.HomeAdminFragment;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AdminDashboardActivity extends FragmentActivity implements
		ActionBar.TabListener {

	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private ViewPager viewPager;
	private ActionBar actionBar;
	private TabsPagerAdminAdapter mAdapter;
	private String[] tabs = { "Home", "Announcement", "Menu", "All Thaali",
			"Settings" };
	private TextView mTitleTextView;
	private TextView tvCountNotification;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;
	private boolean doubleBackToExitPressedOnce;
	public static AdminDashboardActivity activty;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activty = this;
		setContentView(R.layout.activity_user_dashboard);

		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(false);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.blank, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				// invalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources()
				.getColor(R.color.theme_blue)));

		mAdapter = new TabsPagerAdminAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);

		// ///////////////////////////////////////////////////////////////

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.tab_home_selector).setTabListener(this));
		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.tab_announcement_selector)
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.tab_knife_selector).setTabListener(this));
		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.tab_profile_selector).setTabListener(this));
		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.tab_setting_selector).setTabListener(this));

		// /////////////////////////////////////////////////////////////
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.header_action_bar, null);
		mTitleTextView = (TextView) mCustomView.findViewById(R.id.header_text);
		// mTitleTextView.setText("Fawaid");

		LinearLayout llNotification = (LinearLayout) mCustomView
				.findViewById(R.id.ll_actionbar_header_notification);
		tvCountNotification = (TextView) mCustomView
				.findViewById(R.id.tv_actionbar_header_noti);

		llNotification.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AdminDashboardActivity.this,
						NotificatonActivity.class);
				startActivity(intent);
			}
		});

		actionBar.setCustomView(mCustomView);
		actionBar.setDisplayShowCustomEnabled(true);

		// setHomeButtonEnabled enable working of sidebar
		actionBar.setHomeButtonEnabled(false);

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);

		actionBar.setIcon(R.drawable.blank);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		/*
		 * // Adding Tabs for (String tab_name : tabs) {
		 * actionBar.addTab(actionBar.newTab().setText(tab_name)
		 * .setTabListener(this)); }
		 */

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				// actionBar.setSelectedNavigationItem(position);
				//Toast.makeText(getApplicationContext(), "onPageSelected"+position, Toast.LENGTH_LONG).show();
				actionBar.selectTab(actionBar.getTabAt(position));
				mTitleTextView.setText(tabs[position]);

				viewPager.setCurrentItem(position);
				if(AllThaaliFragment.etSearchUser!=null)
				{
					AllThaaliFragment.etSearchUser.setEnabled(false);
					//Toast.makeText(getApplicationContext(), "setEnabled false"+position, Toast.LENGTH_LONG).show();
					getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
				}
				if(position==3)
				{
					AllThaaliFragment.etSearchUser.setEnabled(true);
					
					//Toast.makeText(getApplicationContext(), "setEnabled"+position, Toast.LENGTH_LONG).show();
					
				}/*
				try {
					InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
					if (inputMethodManager != null) {
						if (inputMethodManager.isAcceptingText()) {
							Toast.makeText(getApplicationContext(), "inputMethodManager.isAcceptingText()", Toast.LENGTH_LONG).show();
						}
						if (inputMethodManager.isActive()) {
							Toast.makeText(getApplicationContext(), "inputMethodManager.isAcceptingText()", Toast.LENGTH_LONG).show();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

					if (inputMethodManager != null) {
						if (inputMethodManager.isAcceptingText()) {
							inputMethodManager.hideSoftInputFromWindow(activty.getCurrentFocus().getWindowToken(), 0);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}*/
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		new NotificationCountAsyncTask().execute();
		setFonts();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		if (!this.doubleBackToExitPressedOnce) {
			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Please click BACK again to exit",
					Toast.LENGTH_LONG).show();
			new Handler().postDelayed(new BackPraced(), 2000);
			// break;
		} else {
			super.onBackPressed();
		}
	}

	class BackPraced implements Runnable {
		public void run() {
			doubleBackToExitPressedOnce = false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 2901) {
			if (mAdapter != null) {
				mAdapter.refresh();
			}
		}
		/*
		 * if (requestCode==2902) { if(mAdapter!=null) { mAdapter.refresh(); } }
		 */

	}

	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		mTitleTextView.setTypeface(typefaceBold);
	}

	public void setMenuScreen() {
		actionBar.setSelectedNavigationItem(2);
	}

	public void setAllThaliScreen() {
		actionBar.setSelectedNavigationItem(3);
		System.out.println("Hello 123");
	}

	class NotificationCountAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			try {
				urlParameter = new ArrayList<NameValuePair>();
				urlParameter.add(new BasicNameValuePair("user_id",
						SPAnjuman.getValue(AdminDashboardActivity.this,
								SPAnjuman.UserId)));
				urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
						.getValue(AdminDashboardActivity.this,
								SPAnjuman.MohallaId)));
				Log.v("urlParameter", urlParameter + "");
				progressDialog = new ProgressDialog(AdminDashboardActivity.this); // Create
				progressDialog.setMessage("Loading..."); // Show message in
															// dialog
				progressDialog.show();
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				response = CustomHttpClient.executeHttpPost(
						AdminDashboardActivity.this, WebUrl.NO_THAALI_COUNT,
						urlParameter);

			} catch (Exception e) {

			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				progressDialog.dismiss();
				if (response != null) {

					// llThaliLayout.removeAllViews();
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						jsObject = jsonObject.optJSONObject("result");
						tvCountNotification.setText(jsObject
								.optString("suggestion_count"));
						HomeAdminFragment.getInstance().updateCount(
								jsObject.optString("stop_thaali_count"),
								jsObject.optString("feedback_count"));
					}
				} else {
					Toast.makeText(AdminDashboardActivity.this,
							"Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title

		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		// boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		try {
			viewPager.setCurrentItem(tab.getPosition());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	public void refresh() {
		try {
			new NotificationCountAsyncTask().execute();
		} catch (Exception e) {

		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		activty = null;
	}

}
