package com.faizconnect.adminmodule;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faizconnect.R;
import com.faizconnect.adapter.AdminFawaidTabsPagerAdapter;
import com.faizconnect.adminfragment.AdminJismaniFragment;
import com.faizconnect.adminfragment.AdminRuhaniFragment;
import com.faizconnect.utill.Fonts;

public class AdminFawaidActivity extends FragmentActivity implements
		ActionBar.TabListener {

	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private ViewPager viewPager;
	private ActionBar actionBar;
	private AdminFawaidTabsPagerAdapter mAdapter;
	private String[] tabs = { "Jismani Fawaid", "Ruhani Fawaid"};
	private TextView mTitleTextView;
	private TextView tvCountNotification;
	private Typeface typefaceBold;
	private long ThaliId;
	private String FawaidStatus;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_fawaid);
		
		

		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(false);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.selct_dot, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				// invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}

		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources()
				.getColor(R.color.theme_blue)));
		mAdapter = new AdminFawaidTabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		
		// 		Set Custom View and inflate 
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.header_action_bar, null);
		mTitleTextView = (TextView) mCustomView.findViewById(R.id.header_text);
		mTitleTextView.setText("Fawaid");
		
		mTitleTextView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		LinearLayout llNotification = (LinearLayout) mCustomView.findViewById(R.id.ll_actionbar_header_notification);
		tvCountNotification = (TextView) mCustomView.findViewById(R.id.tv_actionbar_header_noti);
		/*llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UserFawaidActivity.this,UserNotificationActivity.class);
				startActivity(intent);
			}
		});*/
		llNotification.setVisibility(View.GONE);
		
		actionBar.setCustomView(mCustomView);
		actionBar.setDisplayShowCustomEnabled(true);
	
		// setHomeButtonEnabled enable working of sidebar
		actionBar.setHomeButtonEnabled(true);

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);

		actionBar.setIcon(R.drawable.arrow);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		//new NotificationCountAsyncTask().execute();
		setFonts();
	}
	
	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		mTitleTextView.setTypeface(typefaceBold);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		finish();
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
//			Toast.makeText(getApplicationContext(), "Share Option",
//					Toast.LENGTH_LONG).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		// boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new AdminJismaniFragment();
			break;
		case 1:
			fragment = new AdminRuhaniFragment();
			break;

		default:
			break;
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}
	
/*class NotificationCountAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(UserFawaidActivity.this, 
						SPAnjuman.UserId)));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(UserFawaidActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(UserFawaidActivity.this, 
							WebUrl.USER_NOTIFICATION_COUNT, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					jsObject = jsonObject.getJSONObject("result");
					tvCountNotification.setText(jsObject.optString("unread_count"));
				}else {
					Toast.makeText(UserFawaidActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}*/

}
