package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adapter.GridAdapter;
import com.faizconnect.pojo.ChangeMenuPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class ChangeMenuActivity extends Activity {

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private TextView tvAddMore;
	private GridView gridView;
	private String thaliMenuItemId;
	private ArrayList<ChangeMenuPojo> alChangeMenu;
	private GridAdapter adapter;
	private CheckBox cbNoThali;
	private LinearLayout llNoThali;
	private EditText etNoThali;
	private Button btnNoThaliSave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_menu);
		initComponents();
		initListiners();

	}

	private void initComponents() {
		// TODO Auto-generated method stub
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Change Menu");
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		tvAddMore = (TextView) findViewById(R.id.add_more_item);
		gridView = (GridView) findViewById(R.id.change_menu_grid);
		cbNoThali = (CheckBox) findViewById(R.id.cb_change_menu_no_thali);
		llNoThali = (LinearLayout) findViewById(R.id.ll_change_menu_no_thali);
		etNoThali = (EditText) findViewById(R.id.et_change_menu_no_thali_message);
		btnNoThaliSave = (Button) findViewById(R.id.btn_change_menu_no_thali);
		Intent intent = getIntent();
		thaliMenuItemId = intent.getStringExtra("THALI_MENU_ID");
		new GetThaliAsyncTask().execute();
	}

	private void initListiners() {
		// TODO Auto-generated method stub

		tvAddMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ChangeMenuActivity.this,
						AddMenuDialogActivity.class);
				intent.putExtra("THALI_MENU_ID", thaliMenuItemId);
				startActivity(intent);
				finish();
			}
		});

		llNotification.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ChangeMenuActivity.this,
						NotificatonActivity.class);
				startActivity(intent);
			}
		});

		ivBackArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		cbNoThali.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					llNoThali.setVisibility(View.VISIBLE);
					gridView.setVisibility(View.GONE);
					tvAddMore.setVisibility(View.GONE);
				} else {
					llNoThali.setVisibility(View.GONE);
					tvAddMore.setVisibility(View.VISIBLE);
					gridView.setVisibility(View.VISIBLE);
				}
			}
		});
		btnNoThaliSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (etNoThali.getText().toString().trim().length() != 0) {
					new SaveNoThaliAsyncTask().execute();
				} else {
					new ShowDialog().showMsg(ChangeMenuActivity.this,
							"Please Enter Message");
				}
			}
		});
	}

	class GetThaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		private ChangeMenuPojo pojo;

		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("thaali_menu_id",
					thaliMenuItemId));// 25

			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(ChangeMenuActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					ChangeMenuActivity.this, WebUrl.GET_THALI_MENU_ITEM,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				

				JSONObject jsonObject = new JSONObject(response);

				if (jsonObject.optBoolean("status") == true) {

					JSONArray jsonArray = jsonObject.optJSONArray("result");

					alChangeMenu = new ArrayList<ChangeMenuPojo>();
					if(jsonArray==null)
					{
						cbNoThali.setChecked(true);
						llNoThali.setVisibility(View.VISIBLE);
						gridView.setVisibility(View.GONE);
						tvAddMore.setVisibility(View.GONE);
						etNoThali.setText(jsonObject.optString("no_thaali_description"));
					}
					
					if(jsonObject.optString("no_thaali_description").trim().length()>0 && jsonArray.length() == 0)
					{
						cbNoThali.setChecked(true);
						llNoThali.setVisibility(View.VISIBLE);
						gridView.setVisibility(View.GONE);
						tvAddMore.setVisibility(View.GONE);
						etNoThali.setText(jsonObject.optString("no_thaali_description"));
					}

					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						pojo = new ChangeMenuPojo();
						pojo.setItemId(jsObject.optString("item_id"));
						pojo.setItemName(jsObject.optString("title"));
						pojo.setItemImage(jsObject.optString("image"));
						pojo.setThaliMenuId(jsObject.optInt("thaali_menu_id"));

						alChangeMenu.add(pojo);
					}

					adapter = new GridAdapter(ChangeMenuActivity.this,
							alChangeMenu);
					gridView.setAdapter(adapter);

				} else {
					Toast.makeText(ChangeMenuActivity.this,
							jsonObject.optString("message"), Toast.LENGTH_SHORT)
							.show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class SaveNoThaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		private ChangeMenuPojo pojo;

		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("tbl_thaali_menu_id",
					thaliMenuItemId));
			urlParameter.add(new BasicNameValuePair("no_thaali_description",
					etNoThali.getText().toString()));

			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(ChangeMenuActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					ChangeMenuActivity.this, WebUrl.SET_NO_THALI, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				

				JSONObject jsonObject = new JSONObject(response);

				if (jsonObject.optBoolean("status")) {
					finish();
				} else {
					Toast.makeText(ChangeMenuActivity.this,
							jsonObject.optString("message"), Toast.LENGTH_SHORT)
							.show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
