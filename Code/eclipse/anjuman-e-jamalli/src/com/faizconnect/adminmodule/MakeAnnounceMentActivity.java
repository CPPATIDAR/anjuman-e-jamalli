package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.faizconnect.R;
import com.faizconnect.adminmodule.SendMessageActivity.SendMessageAsyncTask;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MakeAnnounceMentActivity extends Activity {

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private EditText etMessage;
	private EditText etTitle;
	private Button btnSend;
	private ShowDialog showMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_make_announcement);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Make Announcement");
		etMessage = (EditText) findViewById(R.id.et_ann_message);
		etTitle = (EditText) findViewById(R.id.et_ann_title);
		btnSend = (Button) findViewById(R.id.btn_announce_send);
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		llNotification.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MakeAnnounceMentActivity.this,
						NotificatonActivity.class);
				startActivity(intent);
			}
		});

		ivBackArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etTitle.getText().toString().length() == 0) {
					showMessage.showMsg(MakeAnnounceMentActivity.this,
							"Please Enter Title.");
				} else if (etMessage.getText().toString().length() == 0) {
					showMessage.showMsg(MakeAnnounceMentActivity.this,
							"Please Enter Message.");
				} else {
					new SendAnnouncementAsyncTask().execute();
				}

			}
		});
	}

	class SendAnnouncementAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter
					.add(new BasicNameValuePair("user_id", SPAnjuman.getValue(
							MakeAnnounceMentActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(MakeAnnounceMentActivity.this,
							SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("title", etTitle
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("announcement", etMessage
					.getText().toString()));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(MakeAnnounceMentActivity.this); // Create
																				// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					MakeAnnounceMentActivity.this, WebUrl.SEND_ANNOUNCEMENT,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				progressDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						Toast.makeText(MakeAnnounceMentActivity.this,
								jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						etMessage.setText("");
						etTitle.setText("");

						finish();
					}
				} else {
					Toast.makeText(MakeAnnounceMentActivity.this,
							"Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
