package com.faizconnect.adminmodule;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.faizconnect.R;
import com.faizconnect.adapter.NoThaaliAdapter;
import com.faizconnect.pojo.NoThaaliPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class NoThaaliTodayActivity extends Activity{
	
	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private ListView listView;
	private ArrayList<NoThaaliPojo> alList;
	private NoThaaliAdapter adapter;
	private ShowDialog showMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_no_thaali_today);
		initComponents();
		initListiners();
		new NoThaaliAsyncTask().execute();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		alList = new ArrayList<NoThaaliPojo>();
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		tvHeaderText.setText("No Thaali Toady");
		listView = (ListView) findViewById(R.id.listview_no_thaali);
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				showMessage.showMsg(NoThaaliTodayActivity.this, 
						alList.get(position).getThaaliReason().toString());
//				Toast.makeText(NoThaaliTodayActivity.this, "Hello", Toast.LENGTH_SHORT).show();
			}
		});
		
		llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NoThaaliTodayActivity.this,NotificatonActivity.class);
				startActivity(intent);
			}
		});
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	class NoThaaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private NoThaaliPojo pojo;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(NoThaaliTodayActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(NoThaaliTodayActivity.this,SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(NoThaaliTodayActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(NoThaaliTodayActivity.this,
					WebUrl.NO_THAALI_TODAY, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					//llThaliLayout.removeAllViews(); 
					 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						for (int i = 0; i < jsonArray.length(); i++) {
							
							jsObject = jsonArray .optJSONObject(i);
							
							pojo = new NoThaaliPojo();
							pojo.setThaaliId(jsObject.optLong("thaali_number"));
							pojo.setThaaliName(jsObject.optString("username"));
							pojo.setThaaliSize(jsObject.optInt("thaali_size"));
							pojo.setThaaliReason(jsObject.optString("reason"));
							pojo.setUserID(jsObject.optLong("user_id"));
								
							alList.add(pojo);
						}
						adapter = new NoThaaliAdapter(NoThaaliTodayActivity.this, alList);
						listView.setAdapter(adapter);
						
					}
				} else {
					Toast.makeText(NoThaaliTodayActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
