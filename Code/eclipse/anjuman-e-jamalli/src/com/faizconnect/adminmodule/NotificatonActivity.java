package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

public class NotificatonActivity extends Activity{

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private PullToRefreshScrollView scrollView;
	private ScrollView mScrollView;
	private LinearLayout llParent;
	private RelativeLayout rlChild;
	private LayoutInflater inflater;
	public int pageCount=1;
	private Typeface typefaceBold;
	private Typeface typefaceNormal;
	private ImageLoader imageLoader;
	private String suggestionId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		imageLoader = new ImageLoader(getApplicationContext());
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("User Messages");
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		llParent = (LinearLayout) findViewById(R.id.ll_parent_admin_noti); 
		scrollView = (PullToRefreshScrollView) findViewById(R.id.scrollview);
		inflater = this.getLayoutInflater();
		new NotificationAsyncTask().execute();
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		scrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				new NotificationAsyncTask().execute();
			}
		});

		 mScrollView = scrollView.getRefreshableView();
	}
class NotificationAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(NotificatonActivity.this, 
						SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id",SPAnjuman.getValue(NotificatonActivity.this, 
					SPAnjuman.MohallaId)));
//			urlParameter.add(new BasicNameValuePair("page_num",pageCount+""));
			//urlParameter.add(new BasicNameValuePair("suggestion_id","10"));nbb
			
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(NotificatonActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(NotificatonActivity.this, 
							WebUrl.ADMIN_GET_NOTIFICATION, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				//
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					JSONArray jsonArray = jsonObject.optJSONArray("result");
//					alNotification = new ArrayList<UserNotificationPojo>();
					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						
						if (i==0) {
							suggestionId = jsObject.optString("suggestion_id");
							Log.v("suggestionId", suggestionId);
							new UpdateReadNotifictionAsyncTask().execute();
						}
						
						rlChild = (RelativeLayout) inflater.inflate(R.layout.notification_item, null);
						TextView tvTitle = (TextView) rlChild.findViewById(R.id.tv_noti_username);
						TextView tvDescripton = (TextView) rlChild.findViewById(R.id.tv_notification_text);
						TextView tvDate = (TextView) rlChild.findViewById(R.id.tv_admin_noti_date);
						TextView tvTifin = (TextView) rlChild.findViewById(R.id.tv_tifin_size);
						ImageView ivUser = (ImageView) rlChild.findViewById(R.id.iv_noti_user_pic);
						ImageView ivCircleframe = (ImageView) rlChild.findViewById(R.id.iv_circle_frame);
						
						
						tvTitle.setTypeface(typefaceBold);
						tvDescripton.setTypeface(typefaceNormal);
						tvDate.setTypeface(typefaceNormal);
						tvTifin.setTypeface(typefaceNormal);
						
						tvTitle.setText(jsObject.optString("username"));
						tvDescripton.setText(jsObject.optString("message"));
						tvTifin.setText(jsObject.optString("thaali_number"));
						imageLoader.DisplayImage(jsObject.optString("image"), R.drawable.ic_launcher, ivUser);
						
						String engDate[] = jsObject.optString("date").split("-");
						int month = Integer.parseInt(engDate[1])-1;
						String hijriDate[] = jsObject.optString("hijari_date").split("-");
						
						//Log.v("month", month+"");
						
						tvDate.setText(hijriDate[0]+" "+jsObject.optString("hijri_month")+" / "
								+engDate[2]+" "+getEnglishIndex(month));
						
						
						if (jsObject.optInt("is_read")==1) {
							rlChild.setBackgroundColor(Color.parseColor("#EFEFEF"));
							ivCircleframe.setImageResource(R.drawable.circle);
							
						} else {
							rlChild.setBackgroundColor(Color.parseColor("#FFFFFF"));
							ivCircleframe.setImageResource(R.drawable.circle_white);
						}
						
						llParent.addView(rlChild);
						
						View view = new View(NotificatonActivity.this);
						view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,1));
						view.setBackgroundColor(Color.parseColor("#999999"));
						llParent.addView(view);
					}
					pageCount++;
					scrollView.onRefreshComplete();
					
				} else {
					Toast.makeText(NotificatonActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
					scrollView.onRefreshComplete();
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}


class UpdateReadNotifictionAsyncTask extends AsyncTask<Void, Void, Void>{
	
	private String response;
	private ArrayList<NameValuePair> urlParameter;
	private JSONObject jsObject;
	private ProgressDialog progressDialog;
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		urlParameter=new ArrayList<NameValuePair>(); 
		urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(NotificatonActivity.this, 
					SPAnjuman.UserId)));
		urlParameter.add(new BasicNameValuePair("mohalla_id",SPAnjuman.getValue(NotificatonActivity.this, 
				SPAnjuman.MohallaId)));
		urlParameter.add(new BasicNameValuePair("suggestion_id",suggestionId));
		Log.v("urlParameter", urlParameter+"");
		progressDialog = new ProgressDialog(NotificatonActivity.this);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		response = CustomHttpClient.executeHttpPost(NotificatonActivity.this, 
				WebUrl.UPDATE_READ_SUGGESTION, urlParameter);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progressDialog.dismiss();
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
public String getEnglishIndex(int month)
{
	String val ="";
	String[] iMonthNames = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"};
	
	val = iMonthNames[month];
	
	return val;
}
}
