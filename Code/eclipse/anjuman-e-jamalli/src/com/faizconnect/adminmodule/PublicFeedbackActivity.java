package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminmodule.NotificatonActivity;
import com.faizconnect.common.AdminMonthlyThaliMenuView;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class PublicFeedbackActivity extends Activity{

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private ShowDialog showMessage;
	private LinearLayout llThaliFeedback;
	private LinearLayout llThaliPeblicView;
	public String thaliMenuItemId;
	private RelativeLayout rlChild;
	private LayoutInflater inflater;
	private Typeface typefaceBold;
	private Typeface typefaceNormal;
	private ImageLoader imageLoader;
	public String thaliId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_public_feedback);
		initComponents();
		initListiners();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		inflater = this.getLayoutInflater();
		imageLoader = new ImageLoader(getApplicationContext());
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Public Feedbacks");
		
		llThaliFeedback  = (LinearLayout) findViewById(R.id.ll_thali_feedback_menu);
		llThaliPeblicView = (LinearLayout) findViewById(R.id.ll_public_view);
		
		Intent intent = getIntent();
		intent.getStringExtra("FeedBack_Status");
		thaliId = intent.getStringExtra("THALI_ID");
		if (intent.getStringExtra("FeedBack_Status").equalsIgnoreCase("Yesterday")) {
			new GetYesterdayThaliFeedbackAsyncTask().execute();
		} else {
			new GetThaliFeedbackAsyncTask().execute();
		}
//		new GetThaliFeedbackAsyncTask().execute();
		
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PublicFeedbackActivity.this,NotificatonActivity.class);
				startActivity(intent);
			}
		});
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	
	class GetYesterdayThaliFeedbackAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(PublicFeedbackActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(PublicFeedbackActivity.this, SPAnjuman.MohallaId)));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(PublicFeedbackActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			response = CustomHttpClient.executeHttpPost(PublicFeedbackActivity.this,
					WebUrl.GET_YESTERDAY_FEEDBACK, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
				
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
						thaliMenuItemId = jsonObject2.optString("thaali_menu_id");
						Log.v("thaliMenuItemId", thaliMenuItemId);
								
						llThaliFeedback.addView(AdminMonthlyThaliMenuView.inflateMenu(PublicFeedbackActivity.this, jsonObject2, true, false,
								R.drawable.menu_button_selector,R.color.menu_button_font,false,false,false));
						
						new GetPublicReviewAsyncTask().execute();
						
					}else{
						showMessage.showMsg(PublicFeedbackActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(PublicFeedbackActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	class GetPublicReviewAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;  

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
//			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
//					.getValue(PublicFeedbackActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("thaalimenu_id", thaliMenuItemId));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(PublicFeedbackActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(PublicFeedbackActivity.this,
					WebUrl.GET_PUBLIC_FEEDBACK, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
				
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONArray jsonArray = jsonObject.optJSONArray("result");
							
						
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							
							/*if (i==0) {
								notificatioId = jsObject.optString("notification_id");
								Log.v("notificatioId", notificatioId);
								new UpdateReadNotifictionAsyncTask().execute();
							}*/
							
							rlChild = (RelativeLayout) inflater.inflate(R.layout.public_feedback_item, null);
							TextView tvTitle = (TextView) rlChild.findViewById(R.id.tv_feedback_username);
							TextView tvDescripton = (TextView) rlChild.findViewById(R.id.tv_feedback_complain);
							TextView tvTifin = (TextView) rlChild.findViewById(R.id.tv_feedback_tifin_size);
							ImageView ivUser = (ImageView) rlChild.findViewById(R.id.iv_feedback_user_pic);
							ImageView imFeedbackFrame = (ImageView) rlChild.findViewById(R.id.im_feedback_frame);
							
							tvTitle.setTypeface(typefaceBold);
							tvDescripton.setTypeface(typefaceNormal);
							tvTifin.setTypeface(typefaceNormal);
							
							tvTitle.setText(jsObject.optString("name"));
							tvDescripton.setText(jsObject.optString("feedback"));
							tvTifin.setText(jsObject.optString("thaali_number"));
							imageLoader.DisplayImage(jsObject.optString("image"), R.drawable.ic_launcher, ivUser);
							
							
							
							if (jsObject.optInt("is_read")==1) {
								rlChild.setBackgroundColor(Color.parseColor("#FFFFFF"));
								imFeedbackFrame.setImageResource(R.drawable.circle_white);
							} else if (jsObject.optInt("is_read")==0){
								imFeedbackFrame.setImageResource(R.drawable.circle_dark);
								rlChild.setBackgroundColor(Color.parseColor("#DADADA"));
								
							}
							
							llThaliPeblicView.addView(rlChild);
							
							View view = new View(PublicFeedbackActivity.this);
							view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,1));
							view.setBackgroundColor(Color.parseColor("#999999"));
							llThaliPeblicView.addView(view);
						}
						
					}else{
						showMessage.showMsg(PublicFeedbackActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(PublicFeedbackActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	class GetThaliFeedbackAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("thaalimenu_id",thaliId));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(PublicFeedbackActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			response = CustomHttpClient.executeHttpPost(PublicFeedbackActivity.this,
					WebUrl.GET_THALI_FEEDBACK, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
						thaliMenuItemId = jsonObject2.optString("thaali_menu_id");
						Log.v("thaliMenuItemId", thaliMenuItemId);
								
						llThaliFeedback.addView(AdminMonthlyThaliMenuView.inflateMenu(PublicFeedbackActivity.this, jsonObject2, true, false,
								R.drawable.menu_button_selector,R.color.menu_button_font,false,false,false));
						
						new GetPublicReviewAsyncTask().execute();
						
					}else{
						showMessage.showMsg(PublicFeedbackActivity.this, jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(PublicFeedbackActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
