package com.faizconnect.adminmodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.LoginActivity;
import com.faizconnect.R;
import com.faizconnect.pojo.NoThaaliPojo;
import com.faizconnect.pojo.ThaliPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class SendMessageActivity extends Activity {

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private Button btnSend;
	private EditText etSubject;
	private EditText etDescription;
	private ImageView ivAddRecipients;
	private ArrayList<ThaliPojo> userData;
	private String sendMsgUserId= "";
	private TextView tvUserText;
	private ShowDialog showMessage;
	ThaliPojo pojo;
	private long tmpId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_message);
		initComponents();
		initListiners();
		
		/*userData = new ArrayList<AddRecipientsPojo>();
		pojo = new AddRecipientsPojo();
		pojo.setRecipientsId(Long.parseLong(sendMsgUserId));
		pojo.setRecipientsName(intent.getStringExtra("NAME"));
		userData.add(pojo);*/
		
		ThaliPojo pojo =(ThaliPojo)getIntent().getSerializableExtra("thali_data");
		if(pojo!=null)
		{
			tvUserText.setText(pojo.getName());
			sendMsgUserId = pojo.getUserId()+"";
			tmpId = pojo.getUserId();
		}else{
			NoThaaliPojo nPojo =(NoThaaliPojo)getIntent().getSerializableExtra("no_thali_data");
			if(nPojo!=null)
			{
				tvUserText.setText(nPojo.getThaaliName());
				sendMsgUserId = nPojo.getUserID()+"";
				tmpId = nPojo.getUserID();
			}
		}
		
		

		
		
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		llNotification.setVisibility(View.GONE);
		tvNotificationText.setVisibility(View.GONE);
		etSubject = (EditText) findViewById(R.id.et_send_subject);
		etDescription = (EditText) findViewById(R.id.et_send_message_disc);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Send Message");
		btnSend = (Button) findViewById(R.id.btn_send_message);
		ivAddRecipients = (ImageView) findViewById(R.id.iv_add_recipients);
		tvUserText = (TextView) findViewById(R.id.tv_select_user);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		ArrayList<ThaliPojo> alRecipients;
		
		if (requestCode == 50) {
			if (data != null) {
				if (data.getSerializableExtra("SelectUser") != null) {
					userData = (ArrayList<ThaliPojo>) data
							.getSerializableExtra("SelectUser");
					sendMsgUserId = "";
					int count=0;
					tvUserText.setText("");
					for (int i = 0; i < userData.size(); i++) {
						if (userData.get(i).getStatus()==1) {
							System.out.println(userData.get(i).getName());
							sendMsgUserId = sendMsgUserId+userData.get(i).getUserId()+",";
							//Log.v("sendMsgUserId", sendMsgUserId);
							count++;
							int total = count-1;
							if (total == 0) {
								tvUserText.setText(userData.get(i).getName().toString());
							} else {
								tvUserText.setText(userData.get(i).getName().toString()+" +"+total);
							}
//							tvUserText.setText(userData.get(i).getRecipientsName().toString()+" "+total+" more others");
						}
					}
					
					//Log.v("finl sendMsgUserId", sendMsgUserId);
					
//					System.out.println(userData.get(0).getRecipientsName());
				}
			}
			
			/*
			 * ArrayList<AddRecipientsPojo> alData=
			 * (ArrayList<AddRecipientsPojo>
			 * )data.getSerializableExtra("SelectUser"); for (int i = 0; i <
			 * alData.size(); i++) { Log.v("ID",
			 * alData.get(i).getRecipientsId()+""); Log.v("Name",
			 * alData.get(i).getRecipientsName()+""); }
			 */
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivAddRecipients.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SendMessageActivity.this,
						AddRecipientsActivity.class);
				//userData.add()
				if(userData!=null)
				{
					intent.putExtra("SelectUser", userData);
				}
				if(tmpId!=0)
				{
					intent.putExtra("single_selection", tmpId);
				}
				startActivityForResult(intent, 50);
			}
		});

		llNotification.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SendMessageActivity.this,
						NotificatonActivity.class);
				startActivity(intent);
			}
		});

		ivBackArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tvUserText.getText().toString().length()==0) {
					showMessage.showMsg(SendMessageActivity.this, "Please Select Recipients");
				}else if (etSubject.getText().toString().length()==0) {
					showMessage.showMsg(SendMessageActivity.this, "Please Enter Subject");
				}else if (etDescription.getText().toString().length()==0) {
					showMessage.showMsg(SendMessageActivity.this, "Please Enter Message");
				} else {
					new SendMessageAsyncTask().execute();
				}
			}
		});
	}

	class SendMessageAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", sendMsgUserId));
			urlParameter.add(new BasicNameValuePair("title", etSubject
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("description",
					etDescription.getText().toString()));
			urlParameter.add(new BasicNameValuePair("created_by",SPAnjuman.getValue(SendMessageActivity.this, SPAnjuman.UserId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(SendMessageActivity.this); // Create
																			// Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					SendMessageActivity.this, WebUrl.ADMIN_SEND_MESSAGE,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						Toast.makeText(SendMessageActivity.this, 
								jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
						etDescription.setText("");
						etSubject.setText("");
						tvUserText.setText("");
						
						finish();

					}
				} else {
					Toast.makeText(SendMessageActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
