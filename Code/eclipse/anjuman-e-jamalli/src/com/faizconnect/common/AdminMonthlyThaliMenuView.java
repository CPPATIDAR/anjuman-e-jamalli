package com.faizconnect.common;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.adminmodule.AddExpences;
import com.faizconnect.adminmodule.AdminFawaidActivity;
import com.faizconnect.adminmodule.ChangeMenuActivity;
import com.faizconnect.adminmodule.PublicFeedbackActivity;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class AdminMonthlyThaliMenuView {
	private static Activity activity;
	private static Typeface typefaceNormal;
	private static Typeface typefaceBold;
	
	public static View inflateMenu(final Activity activity, JSONObject jObject,
			boolean canLike, boolean isFullMenu,int bgselector,int fontSelector,boolean buttonItem,
			boolean menuBorder, boolean showFeedback) {
		menuBorder = true;
		AdminMonthlyThaliMenuView.activity = activity;
		
		typefaceBold = Fonts.BoldFont(activity);
		typefaceNormal = Fonts.normalFont(activity);
		 
		ImageLoader imageLoader = new ImageLoader(activity);
		LayoutInflater layoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View menu = layoutInflater.inflate(R.layout.row_admin_monthly_menu_item, null);
		menu.setTag(jObject);
		TextView tvDay = (TextView) menu
				.findViewById(R.id.tv_admin_row_menu_item_day);
		TextView tvDate = (TextView) menu
				.findViewById(R.id.tv_admin_row_menu_item_date);
		final TextView tvFawaid = (TextView) menu
				.findViewById(R.id.btn_admin_row_menu_item_fawaid);
		
		TextView tvItemPrice = (TextView) menu
		.findViewById(R.id.tv_admin_row_menu_item_price);
		
		((TextView) menu.findViewById(R.id.tv_admin_row_menu_item_currency)).setText(SPAnjuman.getValue(activity, SPAnjuman.CURRENCY));
		TextView tvFeedback = (TextView) menu
				.findViewById(R.id.btn_admin_row_menu_item_feedback);
		TextView tvChangeMenu = (TextView) menu
				.findViewById(R.id.btn_admin_row_menu_item_change_menu);
		
		LinearLayout llItems = (LinearLayout) menu
				.findViewById(R.id.ll_menu_items);
		LinearLayout llRowMenuButton = (LinearLayout) menu
				.findViewById(R.id.ll_row_menu_button);
		ImageView ivDivider  = (ImageView) menu
				.findViewById(R.id.iv_admin_show_divider);
		TextView tvNoThali = (TextView) menu.findViewById(R.id.tv_admin_row_menu_no_thali);
	
		TextView tvNoThaliTitle = (TextView) menu.findViewById(R.id.tv_admin_row_menu_no_thali_title);
		LinearLayout llDate = (LinearLayout) menu.findViewById(R.id.ll_row_menu_date);
		tvFawaid.setTypeface(typefaceBold);
		tvFeedback.setTypeface(typefaceBold);
		tvChangeMenu.setTypeface(typefaceBold);
		//tvDate.setTypeface(typefaceBold);
		tvDay.setTypeface(typefaceNormal);
		
		if (buttonItem) {
			llRowMenuButton.setVisibility(View.VISIBLE);
			ivDivider.setVisibility(View.VISIBLE);
		} else {
			llRowMenuButton.setVisibility(View.GONE);
			ivDivider.setVisibility(View.GONE);
		}
		
		tvFawaid.setBackgroundDrawable(activity.getResources().getDrawable(bgselector));
		tvFawaid.setTextColor(activity.getResources().getColor(fontSelector));
		tvFeedback.setBackgroundDrawable(activity.getResources().getDrawable(bgselector));
		tvFeedback.setTextColor(activity.getResources().getColor(fontSelector));
		tvChangeMenu.setBackgroundDrawable(activity.getResources().getDrawable(bgselector));
		tvChangeMenu.setTextColor(activity.getResources().getColor(fontSelector));
		
		///////////////////
		tvFawaid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				activity.startActivity(new Intent(activity,
						AdminFawaidActivity.class).putExtra("THAALI_ID",
						((Long) v.getTag()).longValue() + "").putExtra("FAWAID_STATUS",tvFawaid.getText().toString()));

			}
		});
		tvFeedback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*Intent intent = new Intent(activity, GiveFeedbackActivity.class);
				intent.putExtra("MENU_JSON", (String) v.getTag());
				activity.startActivityForResult(intent,2906);*/
				
				Intent intent = new Intent(activity, PublicFeedbackActivity.class);
				intent.putExtra("FeedBack_Status", "Thali");
				intent.putExtra("THALI_ID", ((Long) v.getTag()).longValue() + "");
				activity.startActivity(intent);

			}
		});
	
		tvChangeMenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, ChangeMenuActivity.class);
				intent.putExtra("THALI_MENU_ID", ((Long) v.getTag()).longValue() + "");
				activity.startActivityForResult(intent,2901);
			}
		});
		
		tvItemPrice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				activity.startActivityForResult(new Intent(activity,
						AddExpences.class).putExtra("THAALI_ID",
						((Long) v.getTag()).longValue() + ""),2901);

			}
		});
		tvItemPrice.setTag(jObject.optLong("thaali_menu_id"));
		
		
		tvFawaid.setTag(jObject.optLong("thaali_menu_id"));
		tvChangeMenu.setTag(jObject.optLong("thaali_menu_id"));
		tvFeedback.setTag(jObject.optLong("thaali_menu_id"));
		
//		tvFeedback.setTag(jObject.toString());
		
		
		String engDate[] = jObject.optString("date").split("-");
		int month = Integer.parseInt(engDate[1]) - 1;
		String hijriDate[] = jObject.optString("hijari_date").split("-");

		tvDate.setText(hijriDate[2] + " " + jObject.optString("hijari_month")
				+ " / " + engDate[2] + " " + getEnglishIndex(month));
		tvItemPrice.setText(jObject.optString("expense"));
		if (jObject.optString("day").trim().length() == 0) {
			tvDay.setText("Today's Menu");
		}else {
			tvDay.setText(jObject.optString("day"));
		}
		if (jObject.optBoolean("show_feedback")) {
			tvFeedback.setVisibility(View.VISIBLE);
		} else {
			tvFeedback.setVisibility(View.GONE);
		}
	
		
		
		if (showFeedback) {
			
			
			tvFeedback.setVisibility(View.VISIBLE);
			tvChangeMenu.setVisibility(View.GONE);
		}else {
			tvFawaid.setText("ADD FAWAID");
			tvFeedback.setVisibility(View.GONE);
			tvChangeMenu.setVisibility(View.VISIBLE);
		}
		
		JSONArray jsonArray = jObject.optJSONArray("item_data");
		if(jObject.optString("no_thaali_description").trim().length()>0&&jsonArray.length()==0)
		{
			tvNoThali.setVisibility(View.VISIBLE);
			tvNoThali.setText(jObject.optString("no_thaali_description"));
			tvFawaid.setVisibility(View.GONE);
			tvFeedback.setVisibility(View.GONE);
			tvNoThaliTitle.setVisibility(View.VISIBLE);
			tvChangeMenu.setText("ADD MENU");
		}
		if (jsonArray.length() > 0) {
			View items = null;

			LinearLayout llOne = null;
			LinearLayout llTwo = null;
			LinearLayout llThree = null;
			LinearLayout llFour = null;
			
			LinearLayout llOneBorder = null;
			LinearLayout llTwoBorder = null;
			LinearLayout llThreeBorder = null;
			LinearLayout llFourBorder = null;
			

			ImageView ivOne = null;
			ImageView ivTwo = null;
			ImageView ivThree = null;
			ImageView ivFour = null;

			CheckBox cbOne = null;
			CheckBox cbTwo = null;
			CheckBox cbThree = null;
			CheckBox cbFour = null;

			TextView tvOne = null;
			TextView tvTwo = null;
			TextView tvThree = null;
			TextView tvFour = null;
			int i = 0;
			for (i = 0; i < jsonArray.length(); i++) {
				if (i % 4 == 0) {
					items = layoutInflater.inflate(R.layout.row_user_menu_item,
							null);
					llOne = (LinearLayout) items
							.findViewById(R.id.ll_row_menu_item_one);
					llTwo = (LinearLayout) items
							.findViewById(R.id.ll_row_menu_item_two);
					llThree = (LinearLayout) items
							.findViewById(R.id.ll_row_menu_item_three);
					llFour = (LinearLayout) items
							.findViewById(R.id.ll_row_menu_item_four);
					
					llOneBorder = (LinearLayout) items
							.findViewById(R.id.ll_menu1_border);
					llTwoBorder = (LinearLayout) items
							.findViewById(R.id.ll_menu2_border);
					llThreeBorder = (LinearLayout) items
							.findViewById(R.id.ll_menu3_border);
					llFourBorder = (LinearLayout) items
							.findViewById(R.id.ll_menu4_border);

					ivOne = (ImageView) items
							.findViewById(R.id.iv_row_menu_item_image_one);
					ivTwo = (ImageView) items
							.findViewById(R.id.iv_row_menu_item_image_two);
					ivThree = (ImageView) items
							.findViewById(R.id.iv_row_menu_item_image_three);
					ivFour = (ImageView) items
							.findViewById(R.id.iv_row_menu_item_image_four);

					cbOne = (CheckBox) items
							.findViewById(R.id.cb_row_menu_item_like_one);
					cbTwo = (CheckBox) items
							.findViewById(R.id.cb_row_menu_item_like_two);
					cbThree = (CheckBox) items
							.findViewById(R.id.cb_row_menu_item_like_three);
					cbFour = (CheckBox) items
							.findViewById(R.id.cb_row_menu_item_like_four);

					tvOne = (TextView) items
							.findViewById(R.id.tv_row_menu_item_name_one);
					tvTwo = (TextView) items
							.findViewById(R.id.tv_row_menu_item_name_two);
					tvThree = (TextView) items
							.findViewById(R.id.tv_row_menu_item_name_three);
					tvFour = (TextView) items
							.findViewById(R.id.tv_row_menu_item_name_four);
					
					if (isFullMenu) {
						tvOne.setTextColor(Color.parseColor("#333333"));
						tvTwo.setTextColor(Color.parseColor("#333333"));
						tvThree.setTextColor(Color.parseColor("#333333"));
						tvFour.setTextColor(Color.parseColor("#333333"));
					}
					
					if (menuBorder) {
						llOneBorder.setBackgroundResource(R.drawable.border);
						llTwoBorder.setBackgroundResource(R.drawable.border);
						llThreeBorder.setBackgroundResource(R.drawable.border);
						llFourBorder.setBackgroundResource(R.drawable.border);
					}
					
					tvOne.setTypeface(typefaceNormal);
					tvTwo.setTypeface(typefaceNormal);
					tvThree.setTypeface(typefaceNormal);
					tvFour.setTypeface(typefaceNormal);
					cbOne.setButtonDrawable(R.drawable.liked);
					cbTwo.setButtonDrawable(R.drawable.liked);
					cbThree.setButtonDrawable(R.drawable.liked);
					cbFour.setButtonDrawable(R.drawable.liked);

					JSONObject jsonObject = jsonArray.optJSONObject(i);
					if (jsonObject != null) {
						llOne.setVisibility(View.VISIBLE);
						imageLoader.DisplayImage(jsonObject.optString("image"),
								R.drawable.ic_launcher, ivOne);
						if (canLike) {
							cbOne.setVisibility(View.VISIBLE);
						} else {
							cbOne.setVisibility(View.GONE);
						}
						if (jsonObject.optInt("is_like") == 0) {
							cbOne.setChecked(false);
						} else {
							cbOne.setChecked(true);
						}
						cbOne.setTag(jsonObject.optLong("item_id"));
						cbOne.setText(" "+(jsonObject.optLong("like_count")));
						tvOne.setText(jsonObject.optString("title"));
						////////
						/*if(!isFullMenu)
						{
							if (jsonObject.optInt("can_like") == 0) {
								cbOne.setVisibility(View.GONE);
							} else {
								cbOne.setVisibility(View.VISIBLE);
							}
							//can like == 0 hide
						}*/
					}
				}
				if (i % 4 == 1) {
					JSONObject jsonObject = jsonArray.optJSONObject(i);
					if (jsonObject != null) {
						llTwo.setVisibility(View.VISIBLE);
						imageLoader.DisplayImage(jsonObject.optString("image"),
								R.drawable.ic_launcher, ivTwo);
						if (canLike) {
							cbTwo.setVisibility(View.VISIBLE);
						} else {
							cbTwo.setVisibility(View.GONE);
						}
						if (jsonObject.optInt("is_like") == 0) {
							cbTwo.setChecked(false);
						} else {
							cbTwo.setChecked(true);
						}
						cbTwo.setTag(jsonObject.optLong("item_id"));
						cbTwo.setText(" "+(jsonObject.optLong("like_count")));
						tvTwo.setText(jsonObject.optString("title"));
					/*	if(!isFullMenu)
						{
							if (jsonObject.optInt("can_like") == 0) {
								cbTwo.setVisibility(View.GONE);
							} else {
								cbTwo.setVisibility(View.VISIBLE);
							}
							//can like == 0 hide
						}*/
					}
				}
				if (i % 4 == 2) {
					JSONObject jsonObject = jsonArray.optJSONObject(i);
					if (jsonObject != null) {
						llThree.setVisibility(View.VISIBLE);
						imageLoader.DisplayImage(jsonObject.optString("image"),
								R.drawable.ic_launcher, ivThree);
						if (canLike) {
							cbThree.setVisibility(View.VISIBLE);
						} else {
							cbThree.setVisibility(View.GONE);
						}
						if (jsonObject.optInt("is_like") == 0) {
							cbThree.setChecked(false);
						} else {
							cbThree.setChecked(true);
						}
						cbThree.setTag(jsonObject.optLong("item_id"));
						cbThree.setText(" "+(jsonObject.optLong("like_count")));
						tvThree.setText(jsonObject.optString("title"));
						/*if(!isFullMenu)
						{
							if (jsonObject.optInt("can_like") == 0) {
								cbThree.setVisibility(View.GONE);
							} else {
								cbThree.setVisibility(View.VISIBLE);
							}
							//can like == 0 hide
						}*/
						
					}
				}
				if (i % 4 == 3) {
					JSONObject jsonObject = jsonArray.optJSONObject(i);
					if (jsonObject != null) {
						llFour.setVisibility(View.VISIBLE);
						imageLoader.DisplayImage(jsonObject.optString("image"),
								R.drawable.ic_launcher, ivFour);
						if (canLike) {
							cbFour.setVisibility(View.VISIBLE);
						} else {
							cbFour.setVisibility(View.GONE);
						}
						if (jsonObject.optInt("is_like") == 0) {
							cbFour.setChecked(false);
						} else {
							cbFour.setChecked(true);
						}
						cbFour.setTag(jsonObject.optLong("item_id"));
						cbFour.setText(" "+(jsonObject.optLong("like_count")));
						tvFour.setText(jsonObject.optString("title"));
						/*if(!isFullMenu)
						{
							if (jsonObject.optInt("can_like") == 0) {
								cbFour.setVisibility(View.GONE);
							} else {
								cbFour.setVisibility(View.VISIBLE);
							}
							//can like == 0 hide
						}*/
					}
					llItems.addView(items);
				}
				/*
				 * cbOne.setOnCheckedChangeListener(checkChangeListener);
				 * cbTwo.setOnCheckedChangeListener(checkChangeListener);
				 * cbThree.setOnCheckedChangeListener(checkChangeListener);
				 * cbFour.setOnCheckedChangeListener(checkChangeListener);
				 */
				/*cbOne.setOnClickListener(clickListener);
				cbTwo.setOnClickListener(clickListener);
				cbThree.setOnClickListener(clickListener);
				cbFour.setOnClickListener(clickListener);*/
			}

			if (i % 4 != 0) {
				if (items != null) {
					llItems.addView(items);
				}
			}
		}
		return menu;
	}
	
	
	private static String getEnglishIndex(int month) {
		String val = "";
		String[] iMonthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sept", "Oct", "Nov", "Dec" };

		val = iMonthNames[month];

		return val;
	}

/*	static OnCheckedChangeListener checkChangeListener = new CompoundButton.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			new NotificationCountAsyncTask(
					((Long) buttonView.getTag()).longValue(), isChecked)
					.execute();

		}
	};
	static OnClickListener clickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			new NotificationCountAsyncTask(((Long) v.getTag()).longValue(),
					((CheckBox) v).isChecked()).execute();
			if(((CheckBox) v).isChecked())
			{
				((CheckBox) v).setText("Liked");
			}else{
				((CheckBox) v).setText("Like");
			}
			

		}
	};*/

	private static class NotificationCountAsyncTask extends
			AsyncTask<Void, Void, Void> {
		private NotificationCountAsyncTask(long itemId, boolean isChecked) {

			this.itemId = itemId;
			this.isChecked = isChecked;
		}

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		private long itemId;
		private boolean isChecked;

		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(activity, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("item_id", itemId + ""));
			urlParameter.add(new BasicNameValuePair("is_like", isChecked ? "1"
					: "0"));
			progressDialog = new ProgressDialog(activity);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(activity,
					WebUrl.LIKE_MENU_ITEM, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				JSONObject jsonObject = new JSONObject(response);

				if (jsonObject.optBoolean("status")) {

				} else {
					Toast.makeText(activity, "Network Error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
