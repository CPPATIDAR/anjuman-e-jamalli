package com.faizconnect.pojo;

public class AnnouncementPojo {

	private long announceId;
	private String title;
	private String description;
	private String daysAgo;

	public long getAnnounceId() {
		return announceId;
	}

	public void setAnnounceId(long announceId) {
		this.announceId = announceId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDaysAgo() {
		return daysAgo;
	}

	public void setDaysAgo(String daysAgo) {
		this.daysAgo = daysAgo;
	}

}
