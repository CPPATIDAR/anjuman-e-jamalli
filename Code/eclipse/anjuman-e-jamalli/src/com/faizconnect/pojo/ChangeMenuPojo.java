package com.faizconnect.pojo;

public class ChangeMenuPojo {

	private String itemId;
	private String itemName;
	private String itemImage;
	private int thaliMenuId;

	public int getThaliMenuId() {
		return thaliMenuId;
	}

	public void setThaliMenuId(int thaliMenuId) {
		this.thaliMenuId = thaliMenuId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

}
