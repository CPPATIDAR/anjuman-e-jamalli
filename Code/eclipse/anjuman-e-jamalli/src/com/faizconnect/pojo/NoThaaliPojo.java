package com.faizconnect.pojo;

import java.io.Serializable;

public class NoThaaliPojo implements Serializable{

	private long thaaliId;
	private String thaaliName;
	private int thaaliSize;
	private String thaaliReason;
	private long userID;
	
	public long getUserID() {
		return userID;
	}
	public void setUserID(long userID) {
		this.userID = userID;
	}
	public long getThaaliId() {
		return thaaliId;
	}
	public void setThaaliId(long thaaliId) {
		this.thaaliId = thaaliId;
	}
	public String getThaaliName() {
		return thaaliName;
	}
	public void setThaaliName(String thaaliName) {
		this.thaaliName = thaaliName;
	}
	public int getThaaliSize() {
		return thaaliSize;
	}
	public void setThaaliSize(int thaaliSize) {
		this.thaaliSize = thaaliSize;
	}
	public String getThaaliReason() {
		return thaaliReason;
	}
	public void setThaaliReason(String thaaliReason) {
		this.thaaliReason = thaaliReason;
	}

}
