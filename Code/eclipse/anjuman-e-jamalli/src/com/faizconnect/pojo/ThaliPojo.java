package com.faizconnect.pojo;

import java.io.Serializable;

public class ThaliPojo implements Serializable{

	private long userId;
	private String name;
	private int status;
	private long thaliNumber;
	private int thaliSize;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getThaliNumber() {
		return thaliNumber;
	}

	public void setThaliNumber(long thaliNumber) {
		this.thaliNumber = thaliNumber;
	}

	public int getThaliSize() {
		return thaliSize;
	}

	public void setThaliSize(int thaliSize) {
		this.thaliSize = thaliSize;
	}

}
