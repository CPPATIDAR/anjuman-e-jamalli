package com.faizconnect.userfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.faizconnect.LoginActivity;
import com.faizconnect.R;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class UserContactUsFragment extends Fragment {

	private Button btnSend;
	private EditText etMessage;
	private ShowDialog showDialog;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;

	// public UserContactUsFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_contact_us,
				container, false);
		
		showDialog = new ShowDialog();
		btnSend = (Button) rootView.findViewById(R.id.btn_contact_send_message);
		etMessage = (EditText) rootView.findViewById(R.id.et_contact_message);

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (etMessage.getText().toString().length()==0) {
					showDialog.showMsg(getActivity(), "Please Enter Message.");
				} else {
					new SendMessageAsyncTask().execute();
				}

			}
		});

		return rootView;
	}
	
	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getActivity());
		typefaceNormal = Fonts.normalFont(getActivity());
		
		btnSend .setTypeface(typefaceBold);
		etMessage.setTypeface(typefaceNormal);
	}

	class SendMessageAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("message", etMessage
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman.getValue(getActivity(), SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.CONTACT_US, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
//						getActivity().finish();
//						startActivity(getActivity().getIntent());
						Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
						etMessage.setText("");
						
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
