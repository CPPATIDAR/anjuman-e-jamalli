package com.faizconnect.userfragment;


import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.faizconnect.R;
import com.faizconnect.common.ThaliMenuUserView;
import com.faizconnect.usermodule.UserAllAnnouncementActivity;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;

public class UserHomeFragment extends Fragment {
	
	
//	private ArrayList<CustomData> alData;
//	private HomeMenuAdapter adapter;
	private TextView tvToadyDate;
	private String ThaaliMenuItemId;
	private String todayThaaliDate;
	private String todayFeedbackJson;
	private String weekDay;
	private LinearLayout llThaliLayout;
	
	private ViewFlipper viewFlipper;
	private AnimationListener mAnimationListener;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;
	
	 
//	public UserHomeFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_user_home, container, false);
        
        viewFlipper = (ViewFlipper) rootView.findViewById(R.id.view_flipper);
        
        typefaceBold = Fonts.BoldFont(getActivity());
		typefaceNormal = Fonts.normalFont(getActivity());
        
       
        tvToadyDate = (TextView) rootView.findViewById(R.id.tv_feedback_date);
        
        llThaliLayout = (LinearLayout) rootView.findViewById(R.id.table_layout);
        
       
        new TodayThaaliAsyncTask().execute();
         new ShowAnnounceMent().execute();

        
        mAnimationListener = new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}
		};
        
        return rootView;
    }
	
	class ShowAnnounceMent extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(getActivity(), 
						SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("page_num","0"));
			urlParameter.add(new BasicNameValuePair("mohalla_id",SPAnjuman.getValue(getActivity(), 
						SPAnjuman.MohallaId)));
			
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(), 
					WebUrl.USER_ANNOUNCEMENT, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					JSONArray jsonArray = jsonObject.optJSONArray("result");
					LayoutInflater inflater = getActivity().getLayoutInflater();
					
					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						
						LinearLayout llTmp = (LinearLayout) inflater
								.inflate(R.layout.flipper_change, null);
						
						TextView tvBannerTitle = (TextView) llTmp
								.findViewById(R.id.tv_banner_ann_title);
						TextView tvBannerMessage = (TextView) llTmp
								.findViewById(R.id.tv_baneer_announce);
						TextView tvBannerDate = (TextView) llTmp
								.findViewById(R.id.tv_banner_ann_date);
						TextView tvViewAll = (TextView) llTmp
								.findViewById(R.id.tv_banner_view_all);
						LinearLayout llIndicator = (LinearLayout) llTmp
								.findViewById(R.id.ll_home_indicator);
						
						tvBannerTitle.setTypeface(typefaceBold);
						tvViewAll.setTypeface(typefaceBold);
						for(int j = 0 ; j < jsonArray.length();j++)
						{
							ImageView iv = new ImageView(getActivity());
							if(i==j)
							{
								 iv.setImageResource(R.drawable.ind_sel);
							}else{
								 iv.setImageResource(R.drawable.ind_unsel);
							}
							llIndicator.addView(iv);
						}
						
						String engDate[] = jsObject.optString("date").split(" ");
						String dt[] = engDate[0].split("-");
						String time[] = engDate[1].split(":");
						
						int month = Integer.parseInt(dt[1])-1;
						String hijriDate[] = jsObject.optString("hijari_date").split("-");
						
						tvBannerDate.setText(hijriDate[2]+" "+jsObject.optString("hijari_month")+" / "
								+dt[2]+" "+getEnglishIndex(month)+", "+time[0]+":"+time[1]+" "+engDate[2]);
						
						tvBannerTitle.setText(jsObject.optString("title"));
						tvBannerMessage.setText(jsObject.optString("announcement"));
						
						
						/*tvTitle.setText(jsObject.optString("title"));
						tvDiscription.setText(jsObject.optString("announcement"));
						tvDaysAgo.setText(jsObject.optString("daysago"));*/
						
						tvViewAll.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(getActivity(),UserAllAnnouncementActivity.class);
								startActivity(intent);
							}
						});
			
						viewFlipper.addView(llTmp);
					
					}
					
					viewFlipper.setInAnimation(AnimationUtils.loadAnimation(
							getActivity(), R.anim.left_in));
					viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							getActivity(), R.anim.left_out));
					// // controlling animation
					viewFlipper.getInAnimation().setAnimationListener(
							mAnimationListener);

					viewFlipper.setAutoStart(true);
					viewFlipper.setFlipInterval(8000);
					viewFlipper.startFlipping();
					
					
				}else {
//					Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
				}
					
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		
	}

	class TodayThaaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(),SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.HOME_TODAY_THAALI, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					llThaliLayout.removeAllViews(); 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
						
						llThaliLayout.addView(ThaliMenuUserView.inflateMenu(getActivity(), jsonObject2, false, true,
								R.drawable.home_menu_button_selector,R.color.home_menu_button_font,true,false));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getEnglishIndex(int month)
	{
		String val ="";
		String[] iMonthNames = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec" };
		
		val = iMonthNames[month];
		
		return val;
	}
	
	private String getWeek(String date) {
		
		String weekValue = null;
		String week[] = date.split("-");
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(week[0]), Integer.parseInt(week[1])-1, Integer.parseInt(week[2]));
		int weekDay = c.get(Calendar.DAY_OF_WEEK);
		Log.v("WEKDay English======", weekDay+"");
		
		switch (weekDay) {
		case 1:
			weekValue = "SUNDAY";
			break;
		case 2:
			weekValue = "MONDAY";
			break;
		case 3:
			weekValue = "TUESDAY";
			break;
		case 4:
			weekValue = "WEDNESDAY";
			break;
		case 5:
			weekValue = "THURSDAY";
			break;
		case 6:
			weekValue = "FRIDAY";
			break;
		case 7:
			weekValue = "SATURDAY";
			break;

		default:
			break;
		}
		
		return weekValue;
		
	}
	
	
/*	private void BuildTable(int rows, int cols) {

		// outer for loop
		for (int i = 1; i <= rows; i++) {

		TableRow row = new TableRow(getActivity());
		row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
		TableRow.LayoutParams.WRAP_CONTENT));

		// inner for loop
		for (int j = 1; j <= cols; j++) {

		TextView tv = new TextView(getActivity());
		tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
		TableRow.LayoutParams.WRAP_CONTENT));
		//tv.setBackgroundResource(R.drawable.cell_shape);
		tv.setPadding(5, 5, 5, 5);
		tv.setText("R " + i + ", C" + j);

		row.addView(tv);

		}

		tableLayout.addView(row);

		}
		}*/
	public void refresh()
	{
		   new TodayThaaliAsyncTask().execute();
	}
}
