package com.faizconnect.userfragment;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.common.ThaliMenuUserView;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class UserMenuFragment extends Fragment {

	private TextView tvHijriMonth;
	private TextView tvEnglishMonth;
	private ImageView ivLeftArrow;
	private ImageView ivRightArrow;
	private int hijriMonth;
	private int currentHijriMonth;
	private int hijriYear;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;
	private int monthStart;
	private int monthEnd;
	private ShowDialog showMessage;
	private boolean lastHijrtMonthData = false;
	private LinearLayout llMenus;

	public UserMenuFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragment_user_monthly_menu_view, container, false);
		
		showMessage = new ShowDialog();
		
		currentHijriMonth = SPAnjuman.getIntValue(getActivity(), SPAnjuman.HijriMonth);
		hijriMonth = SPAnjuman.getIntValue(getActivity(), SPAnjuman.HijriMonth);
		hijriYear = SPAnjuman.getIntValue(getActivity(), SPAnjuman.HijriYear);
		
		tvHijriMonth = (TextView) rootView.findViewById(R.id.tv_user_monthly_hijri_month);
		tvEnglishMonth = (TextView) rootView.findViewById(R.id.tv_user_monthly_english_month);
		ivLeftArrow = (ImageView) rootView.findViewById(R.id.iv_left_arrow);
		ivRightArrow = (ImageView) rootView.findViewById(R.id.iv_right_arrow);
		
		llMenus = (LinearLayout) rootView.findViewById(R.id.ll_menu_monthly);
		
		setFonts();

		new TodayThaaliAsyncTask().execute();
		
		
		ivLeftArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				lastHijrtMonthData = false;
				if (currentHijriMonth<hijriMonth) {
					if (hijriMonth== 1) {
						hijriMonth = 12;
						hijriYear --;
					}else {
						hijriMonth--;	
					}
					new MonthlyAsyncTask().execute();
					//Log.v("hijriMonth", hijriMonth+"");
					//Log.v("hijriYear", hijriYear+"");
				} else {
					showMessage.showMsg(getActivity(), "You can not see previous month menu.");
				}
				
				/*if (hijriMonth== 1) {
					hijriMonth = 12;
					hijriYear --;
				}else {
					hijriMonth--;	
				}*/
				
				
				
//				new MonthlyAsyncTask().execute();
//				Log.v("hijriMonth", hijriMonth+"");
//				Log.v("hijriYear", hijriYear+"");
			}
		});
		
		ivRightArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (lastHijrtMonthData) {
					showMessage.showMsg(getActivity(), "No Thali menu record available");
				} else {
					if (hijriMonth==12) {
						hijriMonth = 1;
						hijriYear ++;
					}else {
						hijriMonth++;	
					}
					new MonthlyAsyncTask().execute();
				}
				
				
				//new MonthlyAsyncTask().execute();
				
//				Log.v("hijriMonth", hijriMonth+"");
//				Log.v("hijriYear", hijriYear+"");
			}
		});

		return rootView;
	}

	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getActivity());
		typefaceNormal = Fonts.normalFont(getActivity());
		tvHijriMonth.setTypeface(typefaceNormal);
		tvEnglishMonth.setTypeface(typefaceNormal);
	}

	class TodayThaaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
//		private ProgressDialog progressDialog;
		
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("hijari_year", hijriYear+""));
			urlParameter.add(new BasicNameValuePair("hijari_month", hijriMonth+""));
			
			Log.v("urlParameter", urlParameter + "");
//			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
//			progressDialog.setMessage("Loading..."); // Show message in dialog
//			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.USER_MONTHLY_MENU_ITEM, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
//			progressDialog.dismiss();

			try {
				if (response != null) {
				
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONArray jArray = jsonObject.optJSONArray("result");
						
						
					//	for (int i = 0; i < jArray.length(); i++) {
							
//							if (i==0) {
								jsObject = jArray.optJSONObject(0);
								tvHijriMonth.setText(jsObject.optString("hijari_month"));
								String engDate[] = jsObject.optString("date").split("-");
								Log.v("Date==", jsObject.optString("date")+"");
								monthStart = Integer.parseInt(engDate[1])-1;
								Log.v("monthStart", monthStart+"");
//								tvEnglishMonth
//							}
							JSONObject jsObject1 = jArray.optJSONObject(jArray.length()-1);
							
							String engLastDate[] = jsObject1.optString("date").split("-");
							monthEnd = Integer.parseInt(engLastDate[1])-1;
							Log.v("monthEnd", monthEnd+"");
							if (monthStart == monthEnd) {
								tvEnglishMonth.setText(getEnglishIndex(monthStart));
							} else {
								tvEnglishMonth.setText(getEnglishIndex(monthStart)+"-"+
										getEnglishIndex(monthEnd));
							}
						//}
						
						if (jArray != null) {
							if (jArray.length() > 0) {
								llMenus.removeAllViews();
								for(int i = 0 ; i < jArray.length();i++)
								{
								llMenus.addView( ThaliMenuUserView.inflateMenu(getActivity(), jArray.optJSONObject(i), true, false,
										R.drawable.menu_button_selector,R.color.menu_button_font,true,true));
								TextView tv = new TextView(getActivity());
								tv.setText(".");
								tv.setVisibility(View.INVISIBLE);
								llMenus.addView(tv);
								
								}
							}
						}
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	class MonthlyAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.MohallaId)));
			urlParameter.add(new BasicNameValuePair("hijari_year", hijriYear+""));
			urlParameter.add(new BasicNameValuePair("hijari_month", hijriMonth+""));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.USER_MONTHLY_MENU_ITEM, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
				
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONArray jArray = jsonObject.optJSONArray("result");
						
						
					//	for (int i = 0; i < jArray.length(); i++) {
							
//							if (i==0) {
								jsObject = jArray.optJSONObject(0);
								tvHijriMonth.setText(jsObject.optString("hijari_month"));
								String engDate[] = jsObject.optString("date").split("-");
								Log.v("Date==", jsObject.optString("date")+"");
								monthStart = Integer.parseInt(engDate[1])-1;
								Log.v("monthStart", monthStart+"");
//								tvEnglishMonth
//							}
							JSONObject jsObject1 = jArray.optJSONObject(jArray.length()-1);
							
							String engLastDate[] = jsObject1.optString("date").split("-");
							monthEnd = Integer.parseInt(engLastDate[1])-1;
							Log.v("monthEnd", monthEnd+"");
							if (monthStart == monthEnd) {
								tvEnglishMonth.setText(getEnglishIndex(monthStart));
							} else {
								tvEnglishMonth.setText(getEnglishIndex(monthStart)+"-"+
										getEnglishIndex(monthEnd));
							}
						//}
						
						if (jArray != null) {
							if (jArray.length() > 0) {
								llMenus.removeAllViews();
								for(int i = 0 ; i < jArray.length();i++)
								{
								llMenus.addView( ThaliMenuUserView.inflateMenu(getActivity(), jArray.optJSONObject(i), true, false,
										R.drawable.menu_button_selector,R.color.menu_button_font,true,true));
								TextView tv = new TextView(getActivity());
								tv.setText(".");
								tv.setVisibility(View.INVISIBLE);
								llMenus.addView(tv);
								}
							}
						}
					}else{
						lastHijrtMonthData =true;
						hijriMonth--;
						showMessage.showMsg(getActivity(), jsonObject.optString("message"));
//					Toast.makeText(getActivity(), jsonObject.optString("message"),
//							Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getEnglishIndex(int month)
	{
		String val ="";
		String[] iMonthNames = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec" };
		
		val = iMonthNames[month];
		
		return val;
	}

	public void refresh() {
		new TodayThaaliAsyncTask().execute();
		
	}
	
}
