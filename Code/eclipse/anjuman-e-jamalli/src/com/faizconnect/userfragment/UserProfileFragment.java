package com.faizconnect.userfragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.LoginActivity;
import com.faizconnect.R;
import com.faizconnect.imageloader.ImageLoader;
import com.faizconnect.usermodule.UserDashboardActivity;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.EmailValidator;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.MultipartUtility;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class UserProfileFragment extends Fragment {

	private ImageView ivUserPic;
	private TextView tvUsername;
	private EditText etEmail;
	private EditText etPhone;
	private TextView tvSabeelNumber;
	private TextView tvThaliNo;
	private TextView tvThaliSize;
	private EditText etPassword;
	private ImageView ivEditEmail;
	private ImageView ivEditPhone;
	private ImageView ivEditPassword;
	private Button btnSave;
//	private Dialog dialog;
//	private EditText etTextValue;
	private ImageLoader imageLoader;
	private ShowDialog showMessage;

	public static final String IMAGE_DIRECTORY_NAME = "Anjuman-E-Jamali";
	public static final int MEDIA_TYPE_IMAGE = 2;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private Uri fileUri;
	private String selectedImagePath;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;
	private Button btnLogout;
	private String deviceId;
	private TextView tvEjamaat;

	// public UserProfileFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_user_profile,
				container, false);
		showMessage = new ShowDialog();
		imageLoader = new ImageLoader(getActivity());
		tvUsername = (TextView) rootView.findViewById(R.id.tv_prof_username);
		etEmail = (EditText) rootView.findViewById(R.id.et_prof_email);
		etPhone = (EditText) rootView.findViewById(R.id.et_prof_phone);
		tvSabeelNumber = (TextView) rootView
				.findViewById(R.id.tv_prof_sabeel_number);
		tvThaliNo = (TextView) rootView.findViewById(R.id.tv_prof_thali_number);
		tvThaliSize = (TextView) rootView.findViewById(R.id.tv_prof_thali_size);
		etPassword = (EditText) rootView.findViewById(R.id.et_prof_pass);
		ivUserPic = (ImageView) rootView.findViewById(R.id.iv_prof_user_pic);
		btnSave = (Button) rootView.findViewById(R.id.btn_prof_save);
		ivEditEmail = (ImageView) rootView.findViewById(R.id.iv_edit_email);
		ivEditPhone = (ImageView) rootView.findViewById(R.id.iv_edit_phone);
		ivEditPassword = (ImageView) rootView.findViewById(R.id.iv_edit_pass);
		tvEjamaat = (TextView) rootView.findViewById(R.id.tv_prof_e_jamat);
		btnLogout = (Button) rootView.findViewById(R.id.btn_prof_logout);
		setFonts();
		
		etEmail.setEnabled(false);
		etPhone.setEnabled(false);
		etPassword.setEnabled(false);

		new UserProfileAsyncTask().execute();

		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (etEmail.getText().toString().length()!=0&&!EmailValidator.isValid(etEmail.getText().toString())) {
					showMessage.showMsg(getActivity(), "Please Enter Valid E-mail.");
				}else if (etPhone.getText().toString().length()==0) {
					showMessage.showMsg(getActivity(), "Please Enter Phone Number.");
				}else if (etPassword.getText().toString().length()==0) {
					showMessage.showMsg(getActivity(), "Please Enter Password.");
				} else {
					new UpdateProfileAsyncTask().execute();
				}
				//new UpdateProfileAsyncTask().execute();
			}
		});
		
	//tvEmail.setOnTouchListener(this);

		ivEditEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//editDialog(tvEmail);
				
				etEmail.setFocusable(true);
				etEmail.setEnabled(true);
				//tvEmail.setFocusableInTouchMode(true);
				//tvEmail.setCursorVisible(true);
				etEmail.requestFocus();
				etEmail.setSelection(etEmail.getText().length());
				etPhone.setEnabled(false);
				etPassword.setEnabled(false);
				
			}
		});

		ivEditPhone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//editDialog(tvPhone);
				etPhone.setFocusable(true);
				etPhone.setEnabled(true);
				etPhone.requestFocus();
				etPhone.setSelection(etPhone.getText().length());
				etEmail.setEnabled(false);
				etPassword.setEnabled(false);
			}
		});

		ivEditPassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//editDialog(tvPassword);
				
				etPassword.setFocusable(true);
				etPassword.setEnabled(true);
				etPassword.requestFocus();
				etPassword.setSelection(etPassword.getText().length());
				etPassword.setText("");
				etPhone.setEnabled(false);
				etEmail.setEnabled(false);
			}
		});

		ivUserPic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CharSequence colors[] = new CharSequence[] { "Gallery",
						"Camera" };

				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Upload photo from");
				builder.setItems(colors, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							selectImage();
							break;
						case 1:
							captureImage();
							break;
						default:
							break;
						}
					}
				});
				builder.show();

			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new LogoutAsyncTask().execute();
			}
		});
		TelephonyManager manager = (TelephonyManager) getActivity()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();
		
		return rootView;
	}
	
	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getActivity());
		typefaceNormal = Fonts.normalFont(getActivity());
		
		tvUsername .setTypeface(typefaceBold);
		etEmail.setTypeface(typefaceBold);
		etPhone.setTypeface(typefaceBold);
		etPassword.setTypeface(typefaceBold);
		btnSave.setTypeface(typefaceBold);
		tvSabeelNumber.setTypeface(typefaceBold);
		tvThaliNo.setTypeface(typefaceBold);
		tvThaliSize.setTypeface(typefaceBold);
		
	}

	public void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		try {
			intent.putExtra("return-data", true);
			startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void selectImage() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_PICK);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		try {
			intent.putExtra("return-data", true);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {
		Log.e("before directory", "yes");
		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			Log.e("storage directory", "yes");
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	// /////////////////////////////////////////////

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {

				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 8;

				final Bitmap bitmap = BitmapFactory.decodeFile(
						fileUri.getPath(), options);

				ivUserPic.setImageBitmap(bitmap);
				selectedImagePath = fileUri.getPath();

				// //

			} else if (resultCode == Activity.RESULT_CANCELED) {

			} else {
				// failed to capture image
				Toast.makeText(getActivity(), "Sorry! Failed to capture image",
						Toast.LENGTH_SHORT).show();
			}
		} else if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				try {

					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					ivUserPic.setImageBitmap(BitmapFactory
							.decodeFile(selectedImagePath));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null,
				null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	// /////////////////////////////////////////////////////////////////

	class UpdateProfileAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		MultipartUtility mu;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));// 5
			// urlParameter.add(new
			// BasicNameValuePair("user_id",SPAnjuman.getValue(UserProfileActivity.this,
			// SPAnjuman.ID)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
														// box
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {

				mu = new MultipartUtility(WebUrl.UPDATE_USER_PROFILE, "UTF-8");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			mu.addFormField("user_id",
					SPAnjuman.getValue(getActivity(), SPAnjuman.UserId));
			mu.addFormField("email", etEmail.getText().toString());
			mu.addFormField("phone", etPhone.getText().toString());
			mu.addFormField("password", etPassword.getText().toString());
			mu.addFormField("device_type", "android");

			if (selectedImagePath != null) {
				try {
					mu.addFilePart("uploaded_file", selectedImagePath);
					mu.addFormField("user_picture_status", "true");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try {
				response = mu.finish();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.e("responseRegistrationRequest", response);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						Toast.makeText(getActivity(),
								jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						
						etEmail.setEnabled(false);
						etPhone.setEnabled(false);
						etPassword.setEnabled(false);
						
//						getActivity().finish();
//						startActivity(getActivity().getIntent());
//						((UserDashboardActivity) getActivity()).setProfileScreen();
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class UserProfileAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
//		private ProgressDialog progressDialog;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));// 5
			Log.v("urlParameter", urlParameter + "");
//			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
//			progressDialog.setMessage("Loading..."); // Show message in dialog
//														// box
//			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.USER_PROFILE, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						jsObject = jsonObject.optJSONObject("result");

						tvUsername.setText(jsObject.optString("fullname"));
						etEmail.setText(jsObject.optString("email"));
						etPhone.setText(jsObject.optString("phone"));
						tvSabeelNumber.setText(jsObject
								.optString("sabeel_number"));
						tvThaliNo.setText(jsObject.optString("thaali_number"));
						tvThaliSize.setText(jsObject.optString("thaali_size"));
						etPassword.setText(jsObject.optString("password"));
						tvEjamaat.setText(jsObject.optString("ejamaat_id"));
						
						if (jsObject.optString("image").equalsIgnoreCase("")
								|| jsObject.optString("image").equalsIgnoreCase(null)) {
							
						} else {
							imageLoader.DisplayImage(jsObject.optString("image"), R.drawable.ic_launcher, ivUserPic);
						}
//						imageLoader.DisplayImage(jsObject.optString("image"), R.drawable.ic_launcher, ivUserPic);
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	class LogoutAsyncTask extends AsyncTask<Void, Void, Void>{

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("device_id",deviceId));
			
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity());   //   Create Dialog
			progressDialog.setMessage("Loading...");				   //   Show message in dialog box
			progressDialog.show();	
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(), WebUrl.LOGOUT,
					urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		
			
			try {
				progressDialog.dismiss();
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						jsObject = jsonObject.optJSONObject("result");
						SPAnjuman.clear(getActivity());
						startActivity(new Intent(getActivity(),LoginActivity.class));
						getActivity().finish();
					}else {
						new ShowDialog().showMsg(getActivity(), jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
