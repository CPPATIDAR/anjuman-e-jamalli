package com.faizconnect.userfragment;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class UserStopThaaliFragment extends Fragment {

	private Button btnSend;
	private EditText etReason;
	private TextView tvFromDate;
	private TextView tvToDate;
	private String formattedDate;
	private TextView tvIsActive;
	private ShowDialog showMessage;
	public FragmentTransaction ft;
	private Typeface typefaceBold;
	// public UserStopThaaliFragment(){}
	private Typeface typefaceNormal;
	private int todayDate;
	private int todayMonth;
	private int todayYear;
	private long fromMiliSecond;
	private long toMiliSecond;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_stop_thaali,
				container, false);
		showMessage = new ShowDialog();

		Calendar c = Calendar.getInstance();
		todayYear = c.get(Calendar.YEAR);
		todayMonth = c.get(Calendar.MONTH);
		todayDate = c.get(Calendar.DAY_OF_MONTH);

		Log.v("todayYear", todayYear + "");
		Log.v("todayMonth", todayMonth + "");
		Log.v("todayDate", todayDate + "");

		btnSend = (Button) rootView.findViewById(R.id.btn_stop_thaali);
		etReason = (EditText) rootView.findViewById(R.id.et_stop_thaali_reason);
		tvFromDate = (TextView) rootView.findViewById(R.id.tv_from_date);
		tvToDate = (TextView) rootView.findViewById(R.id.tv_to_date);
		setFonts();

		/*
		 * InputMethodManager imm = (InputMethodManager)
		 * getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		 * imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		 * 
		 * //to hide it, call the method again
		 * imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		 */

		tvFromDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getActivity().getFragmentManager(),
						"DatePicker");
				tvIsActive = tvFromDate;
			}
		});

		tvToDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment newFragment = new SelectToDateFragment();
				newFragment.show(getActivity().getFragmentManager(),
						"DatePicker");
				tvIsActive = tvToDate;
			}
		});

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (tvFromDate.getText().toString()
						.equalsIgnoreCase("From Date")) {
					showMessage.showMsg(getActivity(),
							"Please Enter From date.");
				} else if (tvToDate.getText().toString()
						.equalsIgnoreCase("To Date")) {
					showMessage.showMsg(getActivity(), "Please Enter To Date.");
				} else if (etReason.getText().toString().length() == 0) {
					showMessage.showMsg(getActivity(), "Please Enter Reason.");
				} else if ((toMiliSecond <= fromMiliSecond)&&validDiff()) {
					showMessage.showMsg(getActivity(), "Invalid dates.");
				} else {
					new StopThaaliAsyncTask().execute();
				}

			}
		});

		return rootView;
	}
	protected boolean validDiff() {
		long diff = toMiliSecond - fromMiliSecond;
		if (diff<0)
		{
			diff *=-1;
		}
		if(diff<AlarmManager.INTERVAL_HOUR)
		{
			return true;
		}
		return false;
	}
	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getActivity());
		typefaceNormal = Fonts.normalFont(getActivity());
		btnSend.setTypeface(typefaceBold);
		tvFromDate.setTypeface(typefaceNormal);
		tvToDate.setTypeface(typefaceNormal);
	}

	public class SelectDateFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			if (tvFromDate.getText().toString()
					.equalsIgnoreCase(getString(R.string.from_date))) {
				final Calendar calendar = Calendar.getInstance();
				int yy = calendar.get(Calendar.YEAR);
				int mm = calendar.get(Calendar.MONTH);
				int dd = calendar.get(Calendar.DAY_OF_MONTH);
				calendar.set(yy, mm, dd);
				fromMiliSecond = calendar.getTimeInMillis();
				return new DatePickerDialog(getActivity(), this, yy, mm, dd);

			} else {
				String str[] = tvFromDate.getText().toString().split("-");
				final Calendar calendar = Calendar.getInstance();
				int yy = Integer.parseInt(str[2]);
				int mm = Integer.parseInt(str[1]) - 1;
				int dd = Integer.parseInt(str[0]);
				calendar.set(yy, mm, dd);
				fromMiliSecond = calendar.getTimeInMillis();
				return new DatePickerDialog(getActivity(), this, yy, mm, dd);
			}
			// return new DatePickerDialog(getActivity(), this, yy, mm, dd);
			/*
			 * final Calendar calendar = Calendar.getInstance(); int yy =
			 * calendar.get(Calendar.YEAR); int mm =
			 * calendar.get(Calendar.MONTH); int dd =
			 * calendar.get(Calendar.DAY_OF_MONTH); return new
			 * DatePickerDialog(getActivity(), this, yy, mm, dd);
			 */
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {

			populateSetDate(yy, mm + 1, dd, tvIsActive);

		}

		public void populateSetDate(int year, int month, int day, TextView tv) {
			formattedDate = day + "-" + month + "-" + year;

			if (todayYear > year) {
				showMessage.showMsg(getActivity(),
						"You cannot select past date.");
			} else if (todayDate > day && todayMonth + 1 == month
					&& todayYear == year) {
				showMessage.showMsg(getActivity(),
						"You cannot select past date.");
			} else {
				tv.setText(formattedDate);
				final Calendar calendar = Calendar.getInstance();

				if (tv == tvFromDate) {
					calendar.set(year, month - 1, day, 1, 1, 1);
					fromMiliSecond = calendar.getTimeInMillis();
				} else if (tv == tvToDate) {
					calendar.set(year, month - 1, day, 1, 1, 1);
					toMiliSecond = calendar.getTimeInMillis();
				}
			}
		}
	}

	public class SelectToDateFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			if (tvToDate.getText().toString()
					.equalsIgnoreCase(getString(R.string.to_date))) {
				final Calendar calendar = Calendar.getInstance();
				int yy = calendar.get(Calendar.YEAR);
				int mm = calendar.get(Calendar.MONTH);
				int dd = calendar.get(Calendar.DAY_OF_MONTH);
				calendar.set(yy, mm, dd);
				toMiliSecond = calendar.getTimeInMillis();
				return new DatePickerDialog(getActivity(), this, yy, mm, dd);

			} else {
				String str[] = tvToDate.getText().toString().split("-");
				final Calendar calendar = Calendar.getInstance();
				int yy = Integer.parseInt(str[2]);
				int mm = Integer.parseInt(str[1]) - 1;
				int dd = Integer.parseInt(str[0]);
				calendar.set(yy, mm, dd);
				toMiliSecond = calendar.getTimeInMillis();
				return new DatePickerDialog(getActivity(), this, yy, mm, dd);
			}
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {

			populateSetDate(yy, mm + 1, dd, tvIsActive);
		}

		public void populateSetDate(int year, int month, int day, TextView tv) {
			formattedDate = day + "-" + month + "-" + year;

			if (todayYear > year) {
				showMessage.showMsg(getActivity(),
						"You cannot select past date.");
			} else if (todayDate > day && todayMonth + 1 == month
					&& todayYear == year) {
				showMessage.showMsg(getActivity(),
						"You cannot select past date.");
			} else {
				tv.setText(formattedDate);
				final Calendar calendar = Calendar.getInstance();
				if (tv == tvFromDate) {
					calendar.set(year, month - 1, day, 1, 1,1);
					fromMiliSecond = calendar.getTimeInMillis();
				} else if (tv == tvToDate) {
					calendar.set(year, month - 1, day, 1, 1, 1);
					toMiliSecond = calendar.getTimeInMillis();
				}
			}
		}
	}

	class StopThaaliAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(getActivity(), SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("from_date", tvFromDate
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("to_date", tvToDate
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("reason", etReason
					.getText().toString()));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(getActivity()); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(getActivity(),
					WebUrl.STOP_THAALI, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						Toast.makeText(getActivity(),
								jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						etReason.setText("");
						tvFromDate.setText(getString(R.string.from_date));
						tvToDate.setText(getString(R.string.to_date));

						// getActivity().finish();
						// startActivity(getActivity().getIntent());

					} else {
						showMessage.showMsg(getActivity(),
								jsonObject.optString("message"));
					}
				} else {
					Toast.makeText(getActivity(), "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
