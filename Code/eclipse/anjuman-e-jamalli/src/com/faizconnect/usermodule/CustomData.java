package com.faizconnect.usermodule;

/**
 * This is just a simple class for holding data that is used to render our
 * custom view
 */
public class CustomData {

	private String imagePath;
	private String itemName;
	private int itemId;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

}
