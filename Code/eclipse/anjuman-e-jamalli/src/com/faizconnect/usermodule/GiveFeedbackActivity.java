package com.faizconnect.usermodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.common.ThaliMenuUserView;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.ShowDialog;
import com.faizconnect.utill.WebUrl;

public class GiveFeedbackActivity extends Activity{

	private ImageView ivBackArrow;
	private TextView tvNotificationText;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	
	private Button btnSend;
	private EditText etFeedback;
	
	private TextView tvTodayWeekDay;
	
	private ShowDialog showMessage;
	private String thaaliMenuId;
	private LinearLayout llThaliMenu;
	private Typeface typefaceNormal;
	private Typeface typefaceBold;
	private TextView tvFeedbackText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_give_feedback);
		initComponents();
		initListiners();
		setFonts();
		new NotificationCountAsyncTask().execute();
	}
	private void setFonts() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		tvHeaderText.setTypeface(typefaceBold);
		tvFeedbackText.setTypeface(typefaceNormal);
		btnSend.setTypeface(typefaceBold);
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		showMessage = new ShowDialog();
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		tvNotificationText = (TextView) findViewById(R.id.tv_header_noti);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Give Feedbacks");
		btnSend = (Button) findViewById(R.id.btn_give_feedback);
		etFeedback = (EditText) findViewById(R.id.et_feedback_message);
		tvFeedbackText = (TextView) findViewById(R.id.tv_feedback_text);
		
		tvTodayWeekDay = (TextView) findViewById(R.id.tv_current_day);
		llThaliMenu = (LinearLayout) findViewById(R.id.ll_give_feedback_thali_menu);
		
		
		
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(getIntent().getStringExtra("MENU_JSON"));
			thaaliMenuId = jsonObject.optLong("thaali_menu_id")+"";
			/*llThaliMenu.addView(ThaliMenuUserView.inflateMenu(GiveFeedbackActivity.this, jsonObject, true, false,
					R.drawable.menu_button_selector,R.color.menu_button_font,false,false));*/
			
			new GetFeedBackAsyncTask().execute();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(GiveFeedbackActivity.this,UserNotificationActivity.class);
				startActivity(intent);
			}
		});
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etFeedback.getText().toString().length()==0) {
					showMessage.showMsg(GiveFeedbackActivity.this, "Please Enter Message.");
				} else {
					new GiveFeedBackAsyncTask().execute();
				}
				
			}
		});
	}
	
	class GetFeedBackAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(GiveFeedbackActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("thaalimenu_id", thaaliMenuId));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(GiveFeedbackActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(GiveFeedbackActivity.this,
					WebUrl.GET_FEEDBACK, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					 
					//llThaliLayout.removeAllViews(); 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						JSONObject jsonObject2 = jsonObject.optJSONObject("result");
						
						llThaliMenu.addView(ThaliMenuUserView.inflateMenuy(GiveFeedbackActivity.this, jsonObject2, true, false,
								R.drawable.menu_button_selector,R.color.menu_button_font,false,false));
						
//						llThaliLayout.addView(ThaliMenuUserView.inflateMenu(getActivity(), jsonObject2, false, true,
//								R.drawable.home_menu_button_selector,R.color.home_menu_button_font,true,false));
					}
				} else {
					Toast.makeText(GiveFeedbackActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
class NotificationCountAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(GiveFeedbackActivity.this, 
						SPAnjuman.UserId)));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(GiveFeedbackActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(GiveFeedbackActivity.this, 
							WebUrl.USER_NOTIFICATION_COUNT, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					jsObject = jsonObject.getJSONObject("result");
					tvNotificationText.setText(jsObject.optString("unread_count"));
				}else {
					Toast.makeText(GiveFeedbackActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	class GiveFeedBackAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("user_id", SPAnjuman
					.getValue(GiveFeedbackActivity.this, SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("thaali_menu_id", thaaliMenuId));
			urlParameter.add(new BasicNameValuePair("feedback", etFeedback.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mohalla_id", SPAnjuman.getValue(GiveFeedbackActivity.this, SPAnjuman.MohallaId)));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(GiveFeedbackActivity.this); // Create Dialog
			progressDialog.setMessage("Loading..."); // Show message in dialog
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(GiveFeedbackActivity.this,
					WebUrl.USER_THAALI_FEEDBACK, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response != null) {
					 
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						
						Toast.makeText(GiveFeedbackActivity.this, jsonObject.optString("message"),
								Toast.LENGTH_SHORT).show();
						finish();
					}
				} else {
					Toast.makeText(GiveFeedbackActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}
	}
}
