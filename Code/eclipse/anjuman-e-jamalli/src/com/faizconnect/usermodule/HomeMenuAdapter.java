package com.faizconnect.usermodule;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.faizconnect.R;
import com.faizconnect.imageloader.ImageLoader;

/**
 * An array adapter that knows how to render views when given CustomData classes
 */
public class HomeMenuAdapter extends BaseAdapter {
	private ImageLoader imageLoader;
	private ArrayList<CustomData> alData;
	private Activity activity;
	private LayoutInflater layoutInflator;

	public HomeMenuAdapter(Activity activity, ArrayList<CustomData> alData) {
		// super(context, R.layout.custom_data_view, values);
		super();
		this.activity = activity;
		this.alData = alData;
		imageLoader = new ImageLoader(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alData.size();
	}

	@Override
	public CustomData getItem(int position) {
		// TODO Auto-generated method stub
		return alData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;

		if (convertView == null) {
			layoutInflator = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflator.inflate(R.layout.home_user_today_menu,
					null);
			holder = new Holder();

			holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
			holder.ivItem = (ImageView) convertView.findViewById(R.id.iv_today_item);
		} else {
			holder = (Holder) convertView.getTag();
		}
		convertView.setTag(holder);

		holder.tvTitle.setText(alData.get(position).getItemName());
		
		try {
			imageLoader.DisplayImage(alData.get(position).getImagePath(), R.drawable.ic_launcher, holder.ivItem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return convertView;
	}

	/** View holder for the views we need access to */
	private static class Holder {
		public TextView tvTitle;
		public ImageView ivItem;
	}

}
