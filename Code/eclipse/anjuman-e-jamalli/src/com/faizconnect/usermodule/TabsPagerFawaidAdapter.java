package com.faizconnect.usermodule;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.faizconnect.userfragment.UserJismaniFragment;
import com.faizconnect.userfragment.UserRuhaniFragment;

public class TabsPagerFawaidAdapter extends FragmentPagerAdapter {

	public TabsPagerFawaidAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Jismani fragment activity
			return new UserJismaniFragment();
		case 1:

			// Ruhani fragment activity
			return new UserRuhaniFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
