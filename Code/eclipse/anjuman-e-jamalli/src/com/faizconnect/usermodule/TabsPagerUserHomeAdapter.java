package com.faizconnect.usermodule;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.faizconnect.userfragment.UserContactUsFragment;
import com.faizconnect.userfragment.UserHomeFragment;
import com.faizconnect.userfragment.UserMenuFragment;
import com.faizconnect.userfragment.UserProfileFragment;
import com.faizconnect.userfragment.UserStopThaaliFragment;

public class TabsPagerUserHomeAdapter extends FragmentPagerAdapter {
	UserHomeFragment userHomeFragment;
	UserMenuFragment userMenuFragment;
	UserStopThaaliFragment userStopThaaliFragment;
	UserProfileFragment userProfileFragment;
	UserContactUsFragment userContactUsFragment;
	
	
	public TabsPagerUserHomeAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			if(userHomeFragment==null)
			{
				userHomeFragment =  new UserHomeFragment();
			}
			return userHomeFragment;
		case 1:
			// User Menu Monthly Wise fragment activity
			if(userMenuFragment==null)
			{
				userMenuFragment =  new UserMenuFragment();
			}
			return userMenuFragment;
			
		case 2:
			// User Stop Thalli fragment activity
			if(userStopThaaliFragment==null)
			{
				userStopThaaliFragment =  new UserStopThaaliFragment();
			}
			return userStopThaaliFragment;
			
		case 3:
			// USer Profile fragment activity
			if(userProfileFragment==null)
			{
				userProfileFragment =  new UserProfileFragment();
			}
			return userProfileFragment;
		case 4:
			// User Contact fragment activity
			if(userContactUsFragment==null)
			{
				userContactUsFragment =  new UserContactUsFragment();
			}
			return userContactUsFragment;
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 5;
	}
	public void refresh() {
		if(userHomeFragment!=null)
		{
			userHomeFragment.refresh();
		}
		if(userMenuFragment!=null)
		{
			userMenuFragment.refresh();
		}
		
	}
}
