package com.faizconnect.usermodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.pojo.AnnouncementPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

public class UserAllAnnouncementActivity extends Activity{

	private ImageView ivBackArrow;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private TextView tvHeaderNoti;
	private PullToRefreshScrollView scrollView;
	private ScrollView mScrollView;
	private LinearLayout llParent;
	private LinearLayout llChild;
	private LayoutInflater inflater;
	private ArrayList<AnnouncementPojo> alAnnouncement;
	public int pageCount=1;
	private Typeface typefaceBold;
	private Typeface typefaceNormal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_announcement);
		initComponents();
		initListiners();
		//new NotificationCountAsyncTask().execute();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("All Announcements");
		llNotification.setVisibility(View.GONE);
		tvHeaderText.setTypeface(typefaceBold);
		tvHeaderNoti  = (TextView) findViewById(R.id.tv_header_noti);
		tvHeaderNoti.setVisibility(View.GONE);
		scrollView = (PullToRefreshScrollView) findViewById(R.id.scrollview);
		llParent = (LinearLayout) findViewById(R.id.ll_parent_user_announce);
		inflater = this.getLayoutInflater();
		new AnnouncementAsyncTask().execute();
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		llNotification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UserAllAnnouncementActivity.this,UserNotificationActivity.class);
				startActivity(intent);
			}
		});
		
		scrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				new AnnouncementAsyncTask().execute();
			}
		});

		 mScrollView = scrollView.getRefreshableView();
	}
	
	class NotificationCountAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(UserAllAnnouncementActivity.this, 
						SPAnjuman.UserId)));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(UserAllAnnouncementActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(UserAllAnnouncementActivity.this, 
							WebUrl.USER_NOTIFICATION_COUNT, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					jsObject = jsonObject.getJSONObject("result");
					tvHeaderNoti.setText(jsObject.optString("unread_count"));
				}else {
					Toast.makeText(UserAllAnnouncementActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	class AnnouncementAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;
//		private AnnouncementPojo pojo;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(UserAllAnnouncementActivity.this, 
						SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("page_num",pageCount+""));
			urlParameter.add(new BasicNameValuePair("mohalla_id",SPAnjuman.getValue(UserAllAnnouncementActivity.this, 
					SPAnjuman.MohallaId)));
			
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(UserAllAnnouncementActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(UserAllAnnouncementActivity.this, 
							WebUrl.USER_ANNOUNCEMENT, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					JSONArray jsonArray = jsonObject.optJSONArray("result");
					alAnnouncement = new ArrayList<AnnouncementPojo>();
					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						
						llChild = (LinearLayout) inflater.inflate(R.layout.announcement_item, null);
						TextView tvTitle = (TextView) llChild.findViewById(R.id.tv_user_thali_dist);
						TextView tvDaysAgo = (TextView) llChild.findViewById(R.id.tv_user_thali_days_ago);
						TextView tvDiscription = (TextView) llChild.findViewById(R.id.tv_user_ann_desc);
						TextView tvAnnounceDate = (TextView) llChild.findViewById(R.id.tv_announce_date);
						
						
						tvTitle.setTypeface(typefaceBold);
						tvDiscription.setTypeface(typefaceNormal);
						tvDaysAgo.setTypeface(typefaceNormal);
						
//						TextView tvDate = (TextView) llChild.findViewById(R.id.tv_user_noti_date);
						
						tvTitle.setText(jsObject.optString("title"));
						tvDiscription.setText(jsObject.optString("announcement"));
						tvDaysAgo.setText(jsObject.optString("daysago"));
						
						String engDate[] = jsObject.optString("date").split(" ");
						String dateArr[] = engDate[0].split("-");
						String time[] = engDate[1].split(":");
						int month = Integer.parseInt(dateArr[1]) - 1;
						String hijriDate[] = jsObject.optString("hijari_date").split("-");

						tvAnnounceDate.setText(hijriDate[2] + " " + jsObject.optString("hijari_month")
								+ " / " + dateArr[2] + " " + getEnglishIndex(month)+", "+time[0]+":"+time[1]+" "+engDate[2]);
						
						
						
//						tvDaysAgo.setText(jsObject.optString("daysago")+" days ago");
						
						
						/*llChild.setTag(pojo);
						
						llChild.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								UserNotificationPojo notificatonPojo = (UserNotificationPojo) v.getTag();
								Toast.makeText(UserNotificationActivity.this, notificatonPojo.getNotificationId()+"", Toast.LENGTH_SHORT).show();
							}
						});*/
						
						llParent.addView(llChild);
						//alAnnouncement.add(pojo);
						
						/*View view = new View(UserAllAnnouncementActivity.this);
						view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,1));
						view.setBackgroundColor(Color.parseColor("#505050"));
						llParent.addView(view);*/
					}
					pageCount++;
					scrollView.onRefreshComplete();
					
				} else {
					Toast.makeText(UserAllAnnouncementActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
					scrollView.onRefreshComplete();
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	private static String getEnglishIndex(int month) {
		String val = "";
		String[] iMonthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sept", "Oct", "Nov", "Dec" };

		val = iMonthNames[month];

		return val;
	}
}
