package com.faizconnect.usermodule;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizconnect.R;
import com.faizconnect.pojo.UserNotificationPojo;
import com.faizconnect.utill.CustomHttpClient;
import com.faizconnect.utill.Fonts;
import com.faizconnect.utill.SPAnjuman;
import com.faizconnect.utill.WebUrl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

public class UserNotificationActivity extends Activity{
	
	private ImageView ivBackArrow;
	private LinearLayout llNotification;
	private TextView tvHeaderText;
	private PullToRefreshScrollView scrollView;
	private ScrollView mScrollView;
	private LinearLayout llParent;
	private LinearLayout llChild;
	private LayoutInflater inflater;
	private ArrayList<UserNotificationPojo> alNotification;
	public int pageCount=1;
	private String notificatioId;
	private Typeface typefaceBold;
	private Typeface typefaceNormal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_notification);
		initComponents();
		initListiners();
		
	}
	
	private void initComponents() {
		// TODO Auto-generated method stub
		typefaceBold = Fonts.BoldFont(getApplicationContext());
		typefaceNormal = Fonts.normalFont(getApplicationContext());
		ivBackArrow = (ImageView) findViewById(R.id.iv_back_arrow);
		llNotification = (LinearLayout) findViewById(R.id.ll_header_notification);
		tvHeaderText = (TextView) findViewById(R.id.header_text);
		tvHeaderText.setText("Notifications");
		llNotification.setVisibility(View.GONE);
		scrollView = (PullToRefreshScrollView) findViewById(R.id.scrollview);
		llParent = (LinearLayout) findViewById(R.id.ll_parent_user_noti);
		inflater = this.getLayoutInflater();
		tvHeaderText.setTypeface(typefaceBold);
//		listView = (ListView) findViewById(R.id.listview_user_noti);
//		((ListView) findViewById(R.id.listview_user_noti)).setAdapter(new VarshitAdapter(UserNotificationActivity.this,R.layout.user_notification_item));
		new NotificationAsyncTask().execute();
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		ivBackArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
//				Intent intent = new Intent(UserNotificationActivity.this,UserAllAnnouncementActivity.class);
//				startActivity(intent);
			}
		});
		
		tvHeaderText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		scrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				new NotificationAsyncTask().execute();
			}
		});

		 mScrollView = scrollView.getRefreshableView();
	}
	
	public String getEnglishIndex(int month)
	{
		String val ="";
		String[] iMonthNames = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"};
		
		val = iMonthNames[month];
		
		return val;
	}
	
	class NotificationAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;
		private UserNotificationPojo pojo;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-ge nerated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(UserNotificationActivity.this, 
						SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("page_num",pageCount+""));
			
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(UserNotificationActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(UserNotificationActivity.this, 
							WebUrl.USER_NOTIFICATION, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				//
				
				JSONObject jsonObject = new JSONObject(response);
				
				if (jsonObject.optBoolean("status")==true) {
					
					JSONArray jsonArray = jsonObject.optJSONArray("result");
					alNotification = new ArrayList<UserNotificationPojo>();
					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						
						if (i==0) {
							notificatioId = jsObject.optString("notification_id");
							Log.v("notificatioId", notificatioId);
							new UpdateReadNotifictionAsyncTask().execute();
						}
						
						llChild = (LinearLayout) inflater.inflate(R.layout.user_notification_item, null);
						TextView tvTitle = (TextView) llChild.findViewById(R.id.tv_user_noti_title);
						TextView tvDescripton = (TextView) llChild.findViewById(R.id.tv_user_notification_text);
						TextView tvDate = (TextView) llChild.findViewById(R.id.tv_user_noti_date);
						
						tvTitle.setTypeface(typefaceBold);
						tvDescripton.setTypeface(typefaceNormal);
						tvDate.setTypeface(typefaceNormal);
						
						tvTitle.setText(jsObject.optString("title"));
						tvDescripton.setText(jsObject.optString("description"));
						
						String engDate[] = jsObject.optString("date").split("-");
						int month = Integer.parseInt(engDate[1])-1;
						String hijriDate[] = jsObject.optString("hijari_date").split("-");
						
						//Log.v("month", month+"");
						
						tvDate.setText(hijriDate[2]+" "+jsObject.optString("hijari_month")+" / "
								+engDate[2]+" "+getEnglishIndex(month));
						
						pojo = new UserNotificationPojo();
						pojo.setNotificationId(jsObject.optLong("notification_id"));
						pojo.setTitle(jsObject.optString("title"));
						pojo.setDescription(jsObject.optString("description"));
						
						llChild.setTag(pojo);
						
						llChild.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								UserNotificationPojo notificatonPojo = (UserNotificationPojo) v.getTag();
//								Toast.makeText(UserNotificationActivity.this, notificatonPojo.getNotificationId()+"", Toast.LENGTH_SHORT).show();
							}
						});
						
						if (jsObject.optInt("is_viewed")==1) {
							llChild.setBackgroundColor(Color.parseColor("#FFFFFF"));
						} else {
							llChild.setBackgroundColor(Color.parseColor("#DADADA"));
						}
						
						llParent.addView(llChild);
						alNotification.add(pojo);
						
						View view = new View(UserNotificationActivity.this);
						view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,1));
						view.setBackgroundColor(Color.parseColor("#505050"));
						llParent.addView(view);
					}
					pageCount++;
					scrollView.onRefreshComplete();
					
				} else {
					Toast.makeText(UserNotificationActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
					scrollView.onRefreshComplete();
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	class UpdateReadNotifictionAsyncTask extends AsyncTask<Void, Void, Void>{
		
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>(); 
			urlParameter.add(new BasicNameValuePair("user_id",SPAnjuman.getValue(UserNotificationActivity.this, 
						SPAnjuman.UserId)));
			urlParameter.add(new BasicNameValuePair("notification_id",notificatioId));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(UserNotificationActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(UserNotificationActivity.this, 
					WebUrl.USER_UPDATE_READ_NOTIFICATION, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

}
