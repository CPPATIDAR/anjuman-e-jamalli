package com.faizconnect.utill;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;

public class CustomHttpClient {
	private static Activity activity;
	private static NetworkConnection networkConnection;

	public static String executeHttpPost(Activity activity, String url,
			ArrayList<NameValuePair> postParameters) {
		CustomHttpClient.activity = activity;
		String value = "{\"status\":false,\"message\":\"Server Timeout, connection problem, Please try later\"}";
		try {
			networkConnection = new NetworkConnection();
			if (networkConnection.isOnline(activity)) {

				postParameters.add(new BasicNameValuePair("device_type","android"));
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
						postParameters, "UTF-8");
				// entity.setContentEncoding(HTTP.UTF_8);
				post.setEntity(entity);
				HttpResponse result = client.execute(post);

				value = EntityUtils.toString(result.getEntity());
				String s = "";
				for (NameValuePair param : postParameters) {
					s = s + param.getName() + " = " + param.getValue() + " ";
				}
				if (value != null) {
					System.out.println("From " + url + " Paramertes :" + s
							+ " Response : " + value);
					return value;
				} else {

					return value;
				}
			} else {
				return "{\"status\":false,\"message\":\"No Internet Connection\"}";
			}

		} catch (Exception e) {
			e.printStackTrace();

			return value;
		}
	}

	public static String executeHttpGet(String url) {

		try {
			networkConnection = new NetworkConnection();
			if (networkConnection.isOnline(activity)) {
				HttpClient client = new DefaultHttpClient();
				HttpGet get = new HttpGet(url);
				HttpResponse result = client.execute(get);
				String value = EntityUtils.toString(result.getEntity());

				if (value != null) {
					System.out.println("From " + url + " get " + value);
					return value;
				} else {
					System.out.println("From " + url + " get null");
					return null;
				}
			} else {
				return "{\"status\":false,\"message\":\"No Internet Connection\"}";
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("From " + url + " get Exception"
					+ e.getMessage());
			System.out.print(e.getMessage());

			return null;
		}
	}

}