package com.faizconnect.utill;

import android.content.Context;

public class SPAnjuman {

	public static final String UserId = "user_id";
	public static final String MohallaId = "mohalla_id";
	
	public static final String LoginStatus = "login_status";
	public static final String ROLE_ID = "role_id";
	public static final String EJamaatId = "ejamaat_id";
	public static final String HijriMonth = "hijari_month";
	public static final String HijriYear = "hijari_year";
	public static final String ACCOUNT_ACTIVE = "account_active";
	public static final String CURRENCY = "currency";
	
	public static long getLongValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getLong(key, 0);
	}

	public static void setLongValue(Context context, String key, Long value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putLong(key, value).commit();
	}

	public static String getValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getString(key, "");
	}

	public static void setValue(Context context, String key, String value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putString(key, value).commit();
	}

	public static int getIntValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getInt(key, 0);
	}

	public static void setIntValue(Context context, String key, int value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putInt(key, value).commit();
	}

	public static boolean getBooleanValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getBoolean(key, false);
	}

	public static void setBooleanValue(Context context, String key,
			boolean value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putBoolean(key, value).commit();
	}
	public static void clear(Context context) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit().clear().commit();
	}
}
