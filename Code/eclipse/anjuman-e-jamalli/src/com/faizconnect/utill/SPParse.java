package com.faizconnect.utill;

import android.content.Context;

public class SPParse {

	public static final String Parse_Installation_ID = "parse_installation_id";	

	public static String getValue(Context context, String key) {

		return context.getSharedPreferences("PARSE", Context.MODE_PRIVATE)
				.getString(key, "");
	}

	public static void setValue(Context context, String key, String value) {

		context.getSharedPreferences("PARSE", Context.MODE_PRIVATE).edit()
				.putString(key, value).commit();
	}
}
