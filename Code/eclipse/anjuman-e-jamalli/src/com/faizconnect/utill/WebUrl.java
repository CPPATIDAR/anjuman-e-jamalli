package com.faizconnect.utill;

public class WebUrl {

	//public static String BaseUrl = "http://codingserver.com/fmb/admin/webservices/";
	
	public static String BaseUrl = "http://faizconnect.com/admin/webservices/";
	
		
	// public static String BaseUrl =
	// "http://192.168.0.10/fmb/admin/webservices/";

	// User Web-Service
	public static String LOGIN = BaseUrl + "login";
	public static String CONTACT_US = BaseUrl + "saveContactus";
	public static String USER_PROFILE = BaseUrl + "getProfile";
	public static String UPDATE_USER_PROFILE = BaseUrl + "userProfileUpdate";
	public static String STOP_THAALI = BaseUrl + "stopThaali";
	public static String HOME_TODAY_THAALI = BaseUrl + "getTodayThaliItem";
	public static String USER_FAWAID = BaseUrl + "getFawaid";
	public static String USER_THAALI_FEEDBACK = BaseUrl + "userThaliFeedback";
	public static String USER_NOTIFICATION = BaseUrl + "getNotifications";
	public static String USER_UPDATE_READ_NOTIFICATION = BaseUrl
			+ "updateReadNotification";
	public static String USER_ANNOUNCEMENT = BaseUrl + "getAnnouncments";
	public static String USER_NOTIFICATION_COUNT = BaseUrl
			+ "unreadNotificationCount";
	public static String LIKE_MENU_ITEM = BaseUrl + "likeMenuItem";
	public static String USER_MONTHLY_MENU_ITEM = BaseUrl + "getThaliMenuItem";
	public static String APP_SETTING = BaseUrl + "getSettings";
	public static String GET_FEEDBACK = BaseUrl + "getFeedbackThaalimenu";

	// Admin Web-Service
	public static String GET_COUNTRY = BaseUrl + "getCountries";
	public static String GET_STATE = BaseUrl + "getStates";
	public static String GET_CITY = BaseUrl + "getCities";
	public static String GET_MOHALLAH = BaseUrl + "getMohallas";

	public static String NO_THAALI_TODAY = BaseUrl + "getTodaysStopThaali";
	public static String NO_THAALI_COUNT = BaseUrl + "getCountTodaysStopThaali";
	public static String ADMIN_SEND_MESSAGE = BaseUrl + "sendNotificationAdmin";
	public static String ADD_RECIPIENTS = BaseUrl + "addRecipients";
	public static String ADD_NEW_THALI = BaseUrl + "addNewThaali";
	public static String SEND_ANNOUNCEMENT = BaseUrl + "sendAnnouncementAdmin";
	public static String ADMIN_GET_ANNOUNCEMENT = BaseUrl
			+ "getAnnouncementsAdmin";
	public static String ADMIN_GET_NOTIFICATION = BaseUrl + "getUserSuggestion";

	public static String UPDATE_READ_SUGGESTION = BaseUrl + "updateReadSuggestion";
	public static String GET_YESTERDAY_FEEDBACK = BaseUrl
			+ "getYesterdayThaaliMenu";
	public static String GET_PUBLIC_FEEDBACK = BaseUrl + "getPublicFeedback";
	public static String GET_THALI_FEEDBACK = BaseUrl + "getFeedbackThaalimenuAdmin";
	

	public static String GET_THALI_MENU_ITEM = BaseUrl + "getThaalimenuItems";
	public static String DELETE_THALI_ITEM = BaseUrl + "deleteThaalimenuItem";
	public static String ADD_THALI_ITEM = BaseUrl + "addThaalimenuItem";

	// Praveen
	public static String UPDATE_USER_STATUS = BaseUrl + "updateUserStatus";
	public static String GET_THALI_MENU_ITEM_ADMIN = BaseUrl
			+ "getThaliMenuItemAdmin";
	public static String ADMIN_FAWAID = BaseUrl + "getFawaidAdmin";

	public static String UpdateFawaid = BaseUrl + "updateFawaidAdmin";
	public static String GET_EXPENSES = BaseUrl + "getExpenses";

	public static String ADD_EXPENSES = BaseUrl + "addExpenses";

	public static String USER_ACTIVATE_STATUS = BaseUrl + "userActiveStatus";
	 
	public static final String SET_NO_THALI = BaseUrl+"setNoThaali";

	public static final String LOGOUT =  BaseUrl+"logout";

	public static final String UPDATE_THALI_ITEM = BaseUrl+"changeThaalimenuImage";
	
	public static final String SENDER_ID="742092669962";

	public static final String GET_UNIT = BaseUrl+"getUnits";
	public static final String UPDATE_DEVICE_TOKEN = BaseUrl+"updateDeviceToken";
	
		
}
